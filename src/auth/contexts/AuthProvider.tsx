import React, { createContext, useContext } from "react";
import { useLogin } from "../hooks/useLogin";
import { useLogout } from "../hooks/useLogout";
import { useUserInfo } from "../hooks/useUserInfo";
import { useMasterData } from "../hooks/useMasterData";

import {
  ROLE_SUPER_ADMIN_ID as SUPER_ADMIN,
  ROLE_ADMIN_ID as ADMIN,
  ROLE_LEADER_ID as LEADER,
  ROLE_HR_ID as HR,
  ROLE_HRM_ID as HRM,
} from "core/constants/roleID";

interface AuthContextInterface {
  hasRole: (roles?: number[]) => {};
  isLoggingIn: boolean;
  isLoggingOut: boolean;
  login: (email: string, password: string) => Promise<any>;
  logout: () => Promise<any>;
  userInfo?: any;
  listPosition?: any;
  listSkill?: any;
  listLevel?: any;
  listSchool?: any;
  listExperience?: any;
  listPersonnelType?: any;
  listContractType?: any;
  isLeaderPermission?: any;
  isHRPermission?: any;
  isHRMPermission?: any;
}

export const AuthContext = createContext({} as AuthContextInterface);

type AuthProviderProps = {
  children?: React.ReactNode;
};

const AuthProvider = ({ children }: AuthProviderProps) => {
  const { isLoggingIn, login } = useLogin();
  const { isLoggingOut, logout } = useLogout();
  const { data: userInfo } = useUserInfo();
  const {
    listPosition,
    listSkill,
    listLevel,
    listSchool,
    listExperience,
    listPersonnelType,
    listContractType,
  } = useMasterData();

  const hasRole = (roles?: number[]) => {
    if (!roles || roles.length === 0 || !userInfo) {
      return true;
    }
    return roles.includes(userInfo?.role?.id);
  };

  const isLeaderPermission = () => {
    return !![ADMIN, SUPER_ADMIN, LEADER].includes(userInfo?.role?.id);
  };

  const isHRPermission = () => {
    return !![ADMIN, SUPER_ADMIN, HR, HRM].includes(userInfo?.role?.id);
  };

  const isHRMPermission = () => {
    return !![ADMIN, SUPER_ADMIN, HRM].includes(userInfo?.role?.id);
  };

  const handleLogin = async (email: string, password: string) =>
    login({ email, password });

  const handleLogout = async () => logout();

  return (
    <AuthContext.Provider
      value={{
        hasRole,
        isLoggingIn,
        isLoggingOut,
        login: handleLogin,
        logout: handleLogout,
        userInfo,
        listPosition,
        listSkill,
        listLevel,
        listSchool,
        listExperience,
        listPersonnelType,
        listContractType,
        isLeaderPermission,
        isHRPermission,
        isHRMPermission,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export function useAuth() {
  return useContext(AuthContext);
}

export default AuthProvider;
