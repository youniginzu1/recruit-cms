import React from "react";
const Login = React.lazy(() => import("../auth/pages/Login"));
import Dashboard from "../page/Dashboard";
import ManagerCandidate from "../page/ManagerCandidate";
import DetailCandidate from "../page/ManagerCandidate/DetailCandidate";
import ManagerJob from "../page/ManagerJob";
import DetailJob from "../page/ManagerJob/DetailJob";
import ManagerOrder from "../page/ManagerOrder";
import NotFound from "../page/NotFound";
import DecentralizationWrapper from "wrappers/DecentralizationWrapper";
import DetailOrder from "page/ManagerOrder/DetailOrder";

import {
  ROLE_SUPER_ADMIN_ID as SUPER_ADMIN,
  ROLE_ADMIN_ID as ADMIN,
  ROLE_LEADER_ID as LEADER,
  ROLE_HR_ID as HR,
  ROLE_HRM_ID as HRM,
} from "core/constants/roleID";

const routes: any[] = [
  {
    path: "/login",
    element: Login,
    private: false,
  },
  {
    path: "/decentralization",
    name: "Quản lý phân quyền",
    element: DecentralizationWrapper,
    private: true,
    roles: [SUPER_ADMIN, ADMIN],
  },
  {
    path: "/",
    name: "Quản lý ứng viên",
    element: ManagerCandidate,
    private: true,
    roles: [SUPER_ADMIN, ADMIN, LEADER, HRM, HR],
    subPath: [
      {
        path: "/detail-candidate/:id",
        element: DetailCandidate,
        private: true,
      },
    ],
  },
  {
    path: "/manager-job",
    name: "Quản lý job",
    element: ManagerJob,
    private: true,
    roles: [SUPER_ADMIN, ADMIN, HRM, HR],
    subPath: [
      {
        path: "/detail-job/:id",
        element: DetailJob,
        private: true,
      },
    ],
  },
  {
    path: "/manager-order",
    name: "Quản lý order",
    element: ManagerOrder,
    private: true,
    roles: [SUPER_ADMIN, ADMIN, LEADER, HRM, HR],
    subPath: [
      {
        path: "/detail-order/:id",
        element: DetailOrder,
        private: true,
      },
    ],
  },
  {
    path: "*",
    element: NotFound,
    private: false,
  },
];

export default routes;
