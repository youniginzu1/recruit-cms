import HeaderBar from "./components/HeaderBar";
import styled from "styled-components";

const ContainerPage = styled.div`
  background-color: #fff;
  height: 100vh;
`;

interface LayoutProps {
  children: React.ReactElement;
}

const Layout: React.FC<LayoutProps> = ({ children }) => {
  return (
    <ContainerPage>
      <HeaderBar />
      {children}
    </ContainerPage>
  );
};
export default Layout;
