import { useTranslation } from "react-i18next";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useMutation } from "react-query";

import { Modal } from "antd";

import FooterModalSubmit from "core/components/FooterModalSubmit";
import TextField from "core/components/ReactHookForm/TextField";

interface IProps {
  api: any;
  onSuccess?: any;
  open: boolean;
  onCancel: any;
  title: string;
  keyReason: string;
  required?: boolean;
  label?: string;
}

const ModalSubmitReason = (props: IProps) => {
  const { api, open, onSuccess, onCancel, title, keyReason, required, label } =
    props;
  const { t } = useTranslation();

  const {
    handleSubmit,
    control,
    formState: { errors },
  } = useForm<any>({
    resolver: yupResolver(
      yup.object({
        [`${keyReason}`]: yup
          .string()
          .nullable()
          .required(t("validate.required") as string),
      })
    ),
    mode: "onChange",
  });

  const { mutate: submitReason, isLoading: isLoadingSubmit } = useMutation(
    (params: any) => api(params),
    {
      onSuccess: () => onSuccess(),
    }
  );

  return (
    <Modal
      title={title}
      open={open}
      onCancel={onCancel}
      centered
      className="modal-custom"
      footer={
        <FooterModalSubmit
          isLoadingOnOk={isLoadingSubmit}
          onOk={handleSubmit((values) => submitReason(values))}
        />
      }
    >
      <TextField
        name={keyReason}
        control={control}
        errors={errors}
        label={label ?? ""}
        required={required}
        rows={6}
      />
    </Modal>
  );
};

export default ModalSubmitReason;
