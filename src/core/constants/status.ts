import { colors } from "core/assets";

export const ListStatusCandidate = [
  {
    name: "status.passPreliminary",
    status: 7,
    color: colors.processingColor,
  },
  {
    name: "status.waitLeaderApproveCv",
    status: 8,
    color: colors.processingColor,
  },
  {
    name: "status.leaderNotApproveCv",
    status: 9,
    color: colors.lateDeadlineColor,
  },
  {
    name: "status.waitCandidateRespondInvitation",
    status: 10,
    color: colors.processingColor,
  },
  {
    name: "status.waitInterview",
    status: 11,
    color: colors.completedColor,
  },
  // {
  //   name: "status.cancelInterview",
  //   status: 12,
  //   color: colors.lateDeadlineColor,
  // },
  {
    name: "status.waitLeaderApproveCandidate",
    status: 13,
    color: colors.processingColor,
  },
  {
    name: "status.leaderNotApproveCandidate",
    status: 14,
    color: colors.lateDeadlineColor,
  },
  {
    name: "status.waitCandidateRespondOffer",
    status: 15,
    color: colors.processingColor,
  },
  { name: "status.denyOffer", status: 16, color: colors.lateDeadlineColor },
  { name: "status.onboard", status: 17, color: colors.completedColor },
];

export enum ListIdStatusCandidate {
  PASS_PRELIMINARY = 7,
  WAIT_LEADER_APPROVE_CV,
  LEADER_NOT_APPROVE_CV,
  WAIT_CANDIDATE_RESPOND_INVITATION,
  WAIT_INTERVIEW,
  CANCEL_INTERVIEW,
  WAIT_LEADER_APPROVE_CANDIDATE,
  LEADER_NOT_APPROVE_CANDIDATE,
  WAIT_CANDIDATE_RESPOND_OFFER,
  DENY_OFFER,
  ONBOARD,
}

export enum ListStatusOrder {
  WAITING_HR_APPROVE = 1,
  HR_NOT_APPROVE,
  PROCESSING,
  LATE_DEADLINE,
  COMPLETED,
  CANCELED,
}
