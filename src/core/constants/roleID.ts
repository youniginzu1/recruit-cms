export const ROLE_SUPER_ADMIN_ID = 1;
export const ROLE_ADMIN_ID = 3;
export const ROLE_USER_ID = 2;
export const ROLE_LEADER_ID = 6;
export const ROLE_HR_ID = 4;
export const ROLE_HRM_ID = 5;
