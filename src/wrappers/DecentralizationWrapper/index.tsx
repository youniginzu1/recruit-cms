import { useTranslation } from "react-i18next";

import SubHeader from "core/components/SubHeader";
import Account from "page/Account";

export default function DecentralizationWrapper() {
  const [t] = useTranslation();

  return (
    <>
      <SubHeader
        listTab={[
          { label: t("tab.account"), key: "account", element: <Account /> },
        ]}
      />
    </>
  );
}
