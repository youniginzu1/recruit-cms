import { ListStatusOrder } from "core/constants/status";
import { t } from "i18next";
import { colors } from "core/assets";

export const getTextStatusOrder = (status: number | undefined) => {
  switch (status) {
    case ListStatusOrder.WAITING_HR_APPROVE:
      return t("managerOrder.status.waitingHRApprove");
    case ListStatusOrder.HR_NOT_APPROVE:
      return t("managerOrder.status.hrNotApprove");
    case ListStatusOrder.PROCESSING:
      return t("managerOrder.status.processing");
    case ListStatusOrder.LATE_DEADLINE:
      return t("managerOrder.status.lateDeadline");
    case ListStatusOrder.COMPLETED:
      return t("managerOrder.status.completed");
    case ListStatusOrder.CANCELED:
      return t("managerOrder.status.canceled");
    default:
      return null;
  }
};

export const getColorStatusOrder = (status: number | undefined) => {
  switch (status) {
    case ListStatusOrder.WAITING_HR_APPROVE:
      return "gray";
    case ListStatusOrder.HR_NOT_APPROVE:
      return colors.lateDeadlineColor;
    case ListStatusOrder.PROCESSING:
      return colors.processingColor;
    case ListStatusOrder.LATE_DEADLINE:
      return colors.lateDeadlineColor;
    case ListStatusOrder.COMPLETED:
      return colors.completedColor;
    case ListStatusOrder.CANCELED:
      return colors.lateDeadlineColor;
    default:
      return colors.contentForm;
  }
};
