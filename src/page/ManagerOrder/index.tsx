import { useState } from "react";
import { useTranslation } from "react-i18next";
import TableOrder from "./components/TableOrder";
import FilterOrder from "./components/FilterOrder";
import ModalAddOrder from "./components/ModalAddOrder";

import { Button } from "antd";
import { Grid } from "@mui/material";

import { images } from "core/assets";
import styles from "./styles.module.scss";
import { IFilter } from "core/constants/interfaces";
import useFilter from "core/hooks/common/useFilter";
import { useGetListOrder } from "core/hooks/order";

const defaultFilter: IFilter = {
  pageIndex: 1,
  pageSize: 10,
};

const ManagerOrder = () => {
  const { t } = useTranslation();
  const [openModalAddOrder, setOpenModalAddOrder] = useState(false);
  const {
    filter,
    handleFilterChange,
    resetFilter,
    handlePageChange,
    handleRowsPerPageChange,
    handleSearch,
  } = useFilter(defaultFilter);

  const { data: uDataList, isLoading: isLoadingGetList } = useGetListOrder({
    ...filter,
  });

  return (
    <Grid container spacing={2} className={styles.ManagerOrderContainer}>
      <Grid item xs={12} container className="d-flex-sb-c">
        <Grid item className={styles.titleHeader}>
          {t("managerOrder.listOrder")}
        </Grid>
        <Grid item>
          <Button
            className="btn mw-170"
            icon={<img src={images.addIcon} alt="" />}
            onClick={() => setOpenModalAddOrder(true)}
          >
            {t("btn.addOrder")}
          </Button>
        </Grid>
      </Grid>
      <Grid item xs={9.5} className={styles.tableOrderContainer}>
        <TableOrder
          totalItems={uDataList?.totalItems}
          pageSize={uDataList?.pageSize}
          pageIndex={uDataList?.pageIndex}
          data={uDataList?.data}
          isLoading={isLoadingGetList}
          onPageChange={handlePageChange}
          onRowsPerPageChange={handleRowsPerPageChange}
        />
      </Grid>
      <Grid item xs={2.5}>
        <FilterOrder
          handleSearch={handleSearch}
          resetFilter={resetFilter}
          filter={filter}
          handleFilterChange={handleFilterChange}
        />
      </Grid>
      <ModalAddOrder
        open={openModalAddOrder}
        toggle={() => setOpenModalAddOrder(false)}
      />
    </Grid>
  );
};

export default ManagerOrder;
