import { useState } from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";
import { useMutation, useQueryClient } from "react-query";
import styled from "styled-components";

import { Grid } from "@mui/material";
import { Divider, Spin, Button, Popconfirm, message } from "antd";

import { colors } from "core/assets";
import { useAuth } from "auth/contexts/AuthProvider";
import { useGetDetailOrder } from "core/hooks/order";
import BackHeader from "core/components/BackHeader";
import SubHeader from "core/components/SubHeader";
import InfoCard from "core/components/InfoCard";
import { convertUTCToLocalString, formatSalary } from "core/helpers";
import TableCandidate from "./components/TableCandidate";
import TimeLineAction from "./components/TimeLineAction";
import ModalSubmitReason from "core/components/ModalSubmitReason";
import { apiChangeStatusOrder } from "core/api/order";
import { ListStatusOrder } from "core/constants/status";
import { getTextStatusOrder, getColorStatusOrder } from "./helpers";
import {
  USE_GET_DETAIL_ORDER,
  USE_GET_LIST_ORDER,
} from "core/constants/queryName";

const DetailContainer = styled((props) => <Grid container {...props}></Grid>)`
  padding: 20px;
`;

const CustomDivider = styled(Divider)`
  margin: 16px 0px 0px 0px;
  border-top: 2px solid ${colors.borderDividerColor};
`;

const OrderNameContainer = styled((props) => (
  <Grid {...props} container rowSpacing={0.8}></Grid>
))`
  padding-left: 20px !important;
  display: flex;
  align-items: center !important;
`;

const OrderName = styled(Grid)`
  font-size: 16px;
  font-weight: bold;
  color: ${colors.mainColor};
  text-transform: uppercase;
  word-break: break-word;
`;

const TitleInfo = styled.span`
  font-weight: 500;
  font-size: 16px;
  line-height: 19px;
  color: ${colors.contentForm};
`;

const RowInfo = styled((props) => (
  <Grid item xs={12} mt={2} container {...props}>
    <Grid item xs={3.5} sx={{ fontSize: 15, color: colors.titleForm }}>
      {props.title}
    </Grid>
    <Grid
      item
      xs={8.5}
      sx={{
        fontSize: 15,
        color: getColorStatusOrder(props.status),
        fontWeight: 700,
      }}
    >
      {props.content}
    </Grid>
  </Grid>
))`
  padding-left: 50px;
`;

const LeftPageInfo = styled(Grid)`
  padding-left: 25px;
  padding-right: 10px;
  border-left: 1px solid ${colors.borderDividerColor};
`;

interface IProps {}

const DetailOrder = (props: IProps) => {
  const {} = props;
  const { t } = useTranslation();
  const { id } = useParams();
  const queryClient = useQueryClient();
  const { isLeaderPermission, isHRPermission } = useAuth();
  const [isOpenCancelOrder, setOpenCancelOrder] = useState(false);
  const [isOpenCompletedOrder, setOpenCompletedOrder] = useState(false);
  const [isOpenConfirmApprove, setOpenConfirmApprove] = useState(false);
  const [isOpenConfirmNotApprove, setOpenConfirmNotApprove] = useState(false);

  const { data: dataDetail, isLoading: isLoadingGetDetail } =
    useGetDetailOrder(id);

  const onSuccess = () => {
    message.success(t("message.success"));
    queryClient.invalidateQueries([USE_GET_DETAIL_ORDER]);
    queryClient.invalidateQueries([USE_GET_LIST_ORDER]);
  };

  const { mutate: changeStatus, isLoading: isLoadingChangeStatus } =
    useMutation((params: any) => apiChangeStatusOrder(params), {
      onSuccess: () => onSuccess(),
    });

  const MainInfo = () => {
    if (isLoadingGetDetail) return <Spin />;
    return (
      <>
        <RowInfo
          title={t("common.createdDate")}
          content={convertUTCToLocalString(
            dataDetail?.created_at,
            "HH:mm DD/MM/YYYY"
          )}
        />
        <RowInfo
          title={t("common.deadline")}
          content={convertUTCToLocalString(dataDetail?.deadline, "DD/MM/YYYY")}
        />
        <RowInfo
          title={t("common.status")}
          content={getTextStatusOrder(dataDetail?.status)}
          status={dataDetail?.status}
        />
        <RowInfo
          title={t("managerOrder.jobIncludes")}
          content={dataDetail?.job?.name}
        />
        <RowInfo
          title={t("common.position")}
          content={dataDetail?.position?.name}
        />
        <RowInfo title={t("common.skill")} content={dataDetail?.skill?.name} />
        <RowInfo title={t("common.level")} content={dataDetail?.level?.name} />
        <RowInfo
          title={t("managerJob.salaryUpTo")}
          content={formatSalary(dataDetail?.maxSalary)}
        />
        <RowInfo
          title={t("managerJob.totalRecruited")}
          content={dataDetail?.totalRecruited ?? 0 + "/" + dataDetail?.target}
        />
        <RowInfo title={t("common.note")} content={dataDetail?.description} />
      </>
    );
  };

  return (
    <DetailContainer>
      <Grid item xs={12}>
        <BackHeader title={t("text.detail")} />
      </Grid>
      <Grid
        item
        xs={12}
        mt={1.5}
        container
        direction="row"
        alignItems="flex-start"
      >
        <Grid item container xs={8.5}>
          <OrderNameContainer item xs={12} className="d-flex-sb-c">
            <OrderName item>{dataDetail?.name}</OrderName>
            <Grid item className="d-flex-sb-c">
              {isLeaderPermission() &&
                !dataDetail?.totalRecruited &&
                dataDetail?.status !== ListStatusOrder.CANCELED && (
                  <Button
                    className="btn btn--deny mr-10"
                    onClick={() => setOpenCancelOrder(true)}
                  >
                    {t("btn.cancelOrder")}
                  </Button>
                )}
              {isLeaderPermission() && dataDetail?.totalRecruited > 0 && (
                <Button
                  className="btn btn--accept mr-10"
                  onClick={() => setOpenCompletedOrder(true)}
                >
                  {t("btn.completedOrder")}
                </Button>
              )}
              {isHRPermission() &&
                dataDetail?.status === ListStatusOrder.WAITING_HR_APPROVE && (
                  <Popconfirm
                    title={t("alert.areYouSureNotApproveOrder")}
                    open={isOpenConfirmNotApprove}
                    onConfirm={() =>
                      changeStatus({
                        status: ListStatusOrder.HR_NOT_APPROVE,
                        orderId: id,
                        description: "Không duyệt Order",
                      })
                    }
                    onCancel={() => setOpenConfirmNotApprove(false)}
                    okButtonProps={{ loading: isLoadingChangeStatus }}
                  >
                    <Button
                      className="btn btn--deny mr-10"
                      onClick={() => setOpenConfirmNotApprove(true)}
                    >
                      {t("btn.notApproveOrder")}
                    </Button>
                  </Popconfirm>
                )}
              {isHRPermission() &&
                dataDetail?.status === ListStatusOrder.WAITING_HR_APPROVE && (
                  <Popconfirm
                    title={t("alert.areYouSureApproveOrder")}
                    open={isOpenConfirmApprove}
                    onConfirm={() =>
                      changeStatus({
                        status: ListStatusOrder.PROCESSING,
                        orderId: id,
                        description: "Đã duyệt Order",
                      })
                    }
                    onCancel={() => setOpenConfirmApprove(false)}
                    okButtonProps={{ loading: isLoadingChangeStatus }}
                  >
                    <Button
                      className="btn btn--accept mr-10"
                      onClick={() => setOpenConfirmApprove(true)}
                    >
                      {t("btn.approveOrder")}
                    </Button>
                  </Popconfirm>
                )}
            </Grid>
          </OrderNameContainer>
          <CustomDivider />
          <SubHeader
            listTab={[
              {
                label: t("tab.mainInfo"),
                key: "generalInfo",
                element: <MainInfo />,
              },
              {
                label: t("tab.candidateInfo"),
                key: "orderInfo",
                element: <TableCandidate data={dataDetail?.job?.applyInfos} />,
              },
            ]}
          />
        </Grid>
        <LeftPageInfo item xs={3.5}>
          <TitleInfo>{t("managerOrder.creatorOrder")}</TitleInfo>
          <InfoCard
            avatar={dataDetail?.listStatus[0]?.userInfo?.avatar}
            name={dataDetail?.listStatus[0]?.userInfo?.name}
            description={dataDetail?.listStatus[0]?.userInfo?.email}
          />
          {dataDetail?.listStatus?.[1] && (
            <div className="mt-10">
              <TitleInfo>{t("managerOrder.approvePerson")}</TitleInfo>
              <InfoCard
                avatar={dataDetail.listStatus[1]?.userInfo?.avatar}
                name={dataDetail.listStatus[1]?.userInfo?.name}
                description={dataDetail.listStatus[1]?.userInfo?.email}
              />
            </div>
          )}
          <CustomDivider className="mb-10" />
          <TitleInfo>{t("managerOrder.historyAction")}</TitleInfo>
          <TimeLineAction data={dataDetail?.listStatus} />
        </LeftPageInfo>
      </Grid>
      <ModalSubmitReason
        title={t("managerOrder.enterReasonCancelOrder")}
        keyReason="description"
        open={isOpenCancelOrder}
        onCancel={() => setOpenCancelOrder(false)}
        api={(params: any) =>
          apiChangeStatusOrder({
            ...params,
            orderId: id,
            status: ListStatusOrder.CANCELED,
          })
        }
        onSuccess={() => onSuccess()}
        required
      />
      <ModalSubmitReason
        title={t("managerOrder.enterReasonCompletedOrder")}
        keyReason="description"
        open={isOpenCompletedOrder}
        onCancel={() => setOpenCompletedOrder(false)}
        api={(params: any) =>
          apiChangeStatusOrder({
            ...params,
            orderId: id,
            status: ListStatusOrder.COMPLETED,
          })
        }
        onSuccess={() => onSuccess()}
        required
      />
    </DetailContainer>
  );
};

export default DetailOrder;
