import { useTranslation } from "react-i18next";
import styled from "styled-components";

import { Button, Input } from "antd";
import { Grid } from "@mui/material";

import { images } from "core/assets";
import InputFilterByModal from "core/components/InputFilterByModal";
import { useAuth } from "auth/contexts/AuthProvider";
import { ListStatusOrder } from "core/constants/status";

const FilterContainer = styled((props) => <Grid container {...props}></Grid>)`
  max-width: 304px;
  background: #fff;
  border: 1px solid #d6d6d6;
  box-shadow: 0px 5px 25px rgb(34 51 84 / 15%);
  border-radius: 5px;
  padding: 19px;
  padding-bottom: 5px;
`;

interface IProps {
  filter?: any;
  handleSearch?: any;
  resetFilter?: any;
  handleFilterChange?: any;
}

const FilterOrder = (props: IProps) => {
  const { t } = useTranslation();
  const { handleSearch, filter, resetFilter, handleFilterChange } = props;
  const { listPosition, listSkill, listLevel } = useAuth();

  const listStatus = [
    {
      id: ListStatusOrder.WAITING_HR_APPROVE,
      name: t("managerOrder.status.waitingHRApprove"),
    },
    {
      id: ListStatusOrder.HR_NOT_APPROVE,
      name: t("managerOrder.status.hrNotApprove"),
    },
    {
      id: ListStatusOrder.PROCESSING,
      name: t("managerOrder.status.processing"),
    },
    {
      id: ListStatusOrder.LATE_DEADLINE,
      name: t("managerOrder.status.lateDeadline"),
    },
    {
      id: ListStatusOrder.COMPLETED,
      name: t("managerOrder.status.completed"),
    },
    {
      id: ListStatusOrder.CANCELED,
      name: t("managerOrder.status.canceled"),
    },
  ];

  return (
    <FilterContainer>
      <Grid item xs={12} pb={1.5}>
        <Input
          placeholder={t("placeholder.keyword")}
          prefix={<img src={images.searchIcon} />}
          className="inputCustom fs-12"
          onChange={(text) => handleSearch.keywordSearch(text)}
        />
      </Grid>
      <Grid item xs={12} pb={1.5}>
        <InputFilterByModal
          titleModal={t("modal.filterByPosition")}
          placeholderInput={t("common.position")}
          listFilter={listPosition}
          itemIdSelected={filter?.positionId}
          setItemIdSelected={(id: number) =>
            handleFilterChange({ positionId: id })
          }
        />
      </Grid>
      <Grid item xs={12} pb={1.5}>
        <InputFilterByModal
          titleModal={t("modal.filterBySkill")}
          placeholderInput={t("common.skill")}
          listFilter={listSkill}
          itemIdSelected={filter?.skillId}
          setItemIdSelected={(id: number) =>
            handleFilterChange({ skillId: id })
          }
        />
      </Grid>
      <Grid item xs={12} pb={1.5}>
        <InputFilterByModal
          titleModal={t("modal.filterByLevel")}
          placeholderInput={t("common.level")}
          listFilter={listLevel}
          itemIdSelected={filter?.levelId}
          setItemIdSelected={(id: number) =>
            handleFilterChange({ levelId: id })
          }
        />
      </Grid>
      <Grid item xs={12} pb={1.5}>
        <InputFilterByModal
          titleModal={t("modal.filterByStatus")}
          placeholderInput={t("common.status")}
          listFilter={listStatus}
          itemIdSelected={filter?.status}
          setItemIdSelected={(id: number) => handleFilterChange({ status: id })}
        />
      </Grid>
      <Grid item xs={12} pb={1.5}>
        <Button
          className="btn btnForm btn--reset"
          icon={<img src={images.refreshIcon} />}
          onClick={() => resetFilter()}
        >
          {t("btn.reset")}
        </Button>
      </Grid>
    </FilterContainer>
  );
};

export default FilterOrder;
