import { useTranslation } from "react-i18next";
import { useMutation, useQueryClient } from "react-query";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import styled from "styled-components";

import { Grid } from "@mui/material";
import { message, Modal } from "antd";

import { colors } from "core/assets";
import FooterModalSubmit from "core/components/FooterModalSubmit";
import { IOrderForm } from "core/constants/interfaces";
import { apiCreateOrder } from "core/api/order";
import { USE_GET_LIST_ORDER } from "core/constants/queryName";
import TextField from "core/components/ReactHookForm/TextField";
import Autocomplete from "core/components/ReactHookForm/Autocomplete";
import DatePicker from "core/components/ReactHookForm/DatePicker";
import { useAuth } from "auth/contexts/AuthProvider";
import moment from "moment";

const TitleInfo = styled.span`
  font-weight: 700;
  font-size: 16px;
  line-height: 19px;
  color: ${colors.mainColor};
`;

interface IProps {
  open: boolean;
  toggle: () => void;
}
const ModalAddOrder = (props: IProps) => {
  const { open, toggle } = props;
  const { t } = useTranslation();
  const queryClient = useQueryClient();
  const { listPosition, listSkill, listLevel } = useAuth();

  const {
    handleSubmit,
    control,
    setValue,
    clearErrors,
    formState: { errors },
  } = useForm<IOrderForm>({
    resolver: yupResolver(
      yup.object({
        name: yup
          .string()
          .nullable()
          .required(t("validate.required") as string)
          .min(6, t("validate.nameMin6") as string),
        positionId: yup
          .string()
          .required(t("validate.required") as string)
          .nullable(),
        skillId: yup
          .string()
          .required(t("validate.required") as string)
          .nullable(),
        levelId: yup
          .string()
          .required(t("validate.required") as string)
          .nullable(),
        maxSalary: yup
          .number()
          .required(t("validate.required") as string)
          .typeError(t("validate.enterCorrectFormat") as string),
        target: yup
          .number()
          .required(t("validate.required") as string)
          .typeError(t("validate.enterCorrectFormat") as string),
      })
    ),
    mode: "onChange",
  });

  const { mutate: createOrder, isLoading: isLoadingCreateOrder } = useMutation(
    (params: any) => apiCreateOrder(params),
    {
      onSuccess: () => {
        queryClient.invalidateQueries([USE_GET_LIST_ORDER]);
        message.success(t("message.addOrderSuccess"));
        toggle();
      },
    }
  );

  return (
    <Modal
      title={t("modal.addOrderTitle")}
      className="modal-custom"
      open={open}
      centered
      onCancel={toggle}
      footer={
        <FooterModalSubmit
          onOk={handleSubmit((values) => createOrder(values))}
          onCancel={toggle}
          isLoadingOnOk={isLoadingCreateOrder}
        />
      }
    >
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <TitleInfo>{t("form.generalInfoTitle")}</TitleInfo>
        </Grid>
        <Grid item xs={12}>
          <TextField
            name="name"
            label={t("managerOrder.titleRequest")}
            errors={errors}
            control={control}
            required
          />
        </Grid>
        <Grid item xs={6}>
          <Autocomplete
            name="positionId"
            label={t("common.position")}
            control={control}
            errors={errors}
            clearErrors={clearErrors}
            listOptions={listPosition}
            getOptionLabel={(option) => option.name}
            customRenderOption={(option) => <span>{option.name}</span>}
            onChange={(option) => setValue("positionId", option?.id ?? null)}
            required
          />
        </Grid>
        <Grid item xs={6}>
          <Autocomplete
            name="skillId"
            label={t("common.skill")}
            control={control}
            errors={errors}
            clearErrors={clearErrors}
            listOptions={listSkill}
            getOptionLabel={(option) => option.name}
            customRenderOption={(option) => <span>{option.name}</span>}
            onChange={(option) => setValue("skillId", option?.id ?? null)}
            required
          />
        </Grid>
        <Grid item xs={6}>
          <Autocomplete
            name="levelId"
            label={t("common.level")}
            control={control}
            errors={errors}
            clearErrors={clearErrors}
            listOptions={listLevel}
            getOptionLabel={(option) => option.name}
            customRenderOption={(option) => <span>{option.name}</span>}
            onChange={(option) => setValue("levelId", option?.id ?? null)}
            required
          />
        </Grid>
        <Grid item xs={6}>
          <TextField
            name="target"
            label={t("managerOrder.targetRecruit")}
            errors={errors}
            control={control}
            type="number"
            required
          />
        </Grid>
        <Grid item xs={12}>
          <DatePicker
            name="deadline"
            label={t("common.deadline")}
            control={control}
            errors={errors}
            minDate={moment()}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            name="maxSalary"
            label={t("managerJob.salaryUpTo")}
            errors={errors}
            control={control}
            type="number"
            required
          />
        </Grid>
        <Grid item xs={12}>
          <TitleInfo>{t("form.description")}</TitleInfo>
        </Grid>
        <Grid item xs={12}>
          <TextField
            name="description"
            errors={errors}
            control={control}
            rows={3}
          />
        </Grid>
      </Grid>
    </Modal>
  );
};

export default ModalAddOrder;
