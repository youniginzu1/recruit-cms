import { useTranslation } from "react-i18next";
import styled from "styled-components";

import { Table } from "antd";
import { ColumnsType } from "antd/lib/table";

import { colors } from "core/assets";
import { ICandidateInfo } from "core/constants/interfaces";
import {
  getColorStatusCandidate,
  getTextStatusCandidate,
} from "page/ManagerCandidate/helpers";

interface IProps {
  data: any;
}

const TitleInfo = styled.span`
  font-weight: 700;
  font-size: 16px;
  line-height: 19px;
  color: ${colors.mainColor};
`;

const TableCandidateInOrder = (props: IProps) => {
  const { data } = props;
  const { t } = useTranslation();
  const columns: ColumnsType<ICandidateInfo> = [
    {
      title: t("table.index"),
      dataIndex: "index",
      key: "index",
      render: (value, record, index) => <span>{index + 1}</span>,
      width: "5%",
      align: "center",
    },
    {
      title: t("managerCandidate.candidateName"),
      dataIndex: "fullName",
      render: (value, record, index) => (
        <span>{record?.candidate?.fullName}</span>
      ),
      width: "26%",
    },
    {
      title: t("common.status"),
      dataIndex: "status",
      render: (value, record, index) => (
        <span style={{ color: getColorStatusCandidate(record?.status) }}>
          {t(getTextStatusCandidate(record?.status) || 'N/A')}
        </span>
      ),
      width: "26%",
    },
    {
      title: t("managerCandidate.HRAssigned"),
      dataIndex: "hrAssigned",
      render: (value, record, index) => (
        <span>{record?.candidate?.hrAssigned?.name}</span>
      ),
      width: "26%",
    },
    {
      title: t("managerCandidate.linkCandidate"),
      dataIndex: "link",
      render: (value, record, index) => (
        <a href={`/detail-candidate/${record?.id}`}>
          {t("common.seeDetail")}
        </a>
      ),
      align: "center",
    },
  ];

  return (
    <div>
      <TitleInfo>
        {t("text.totalCandidate") + ": " + (data?.length ?? 0)}
      </TitleInfo>
      <Table
        columns={columns}
        dataSource={data}
        pagination={false}
        bordered
        rowKey={(obj) => obj.id}
        className="tableCus table--rowSelect mt-10"
      />
    </div>
  );
};

export default TableCandidateInOrder;
