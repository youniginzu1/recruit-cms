import styled from "styled-components";
import { useTranslation } from "react-i18next";
import { IUser } from "core/constants/interfaces";
import { images } from "core/assets";
import { convertUTCToLocalString } from "core/helpers";
import { colors } from "core/assets";

import Timeline from "@mui/lab/Timeline";
import TimelineItem from "@mui/lab/TimelineItem";
import TimelineSeparator from "@mui/lab/TimelineSeparator";
import TimelineConnector from "@mui/lab/TimelineConnector";
import TimelineContent from "@mui/lab/TimelineContent";
import TimelineDot from "@mui/lab/TimelineDot";
import TimelineOppositeContent from "@mui/lab/TimelineOppositeContent";
import InfoCard from "core/components/InfoCard";

const CustomTimelineContainer = styled(Timeline)`
  max-height: 450px;
  overflow-y: scroll;
  overflow-x: hidden;
  &::-webkit-scrollbar {
    width: 2px;
  }

  &::-webkit-scrollbar-thumb {
    border-radius: 30px;
    background-color: ${colors.mainColorRGB};
  }
`;

const CustomTimelineDot = styled(TimelineDot)`
  background-color: transparent !important;
  margin: 0 !important;
`;

const CustomTimelineContent = styled(TimelineContent)`
  padding: 0px 16px 15px 16px !important;
`;

const TimeCreated = styled.span`
  display: block;
  font-weight: 400;
  font-size: 14px;
  color: #8d8d8d;
`;

const Note = styled.span`
  font-weight: 400;
  font-size: 15px;
  color: #223354;
`;

interface IProps {
  data: {
    note?: string;
    created_at?: string;
    status?: number;
    userInfo?: IUser;
  }[];
}

const TimeLineAction = (props: IProps) => {
  const { data } = props;
  return (
    <CustomTimelineContainer>
      {data?.map((item, index) => {
        return (
          <TimelineItem key={index}>
            <TimelineOppositeContent sx={{ display: "none" }} />
            <TimelineSeparator>
              <CustomTimelineDot>
                <img src={images.iconCheckedCircle} alt="" />
              </CustomTimelineDot>
              <TimelineConnector />
            </TimelineSeparator>
            <CustomTimelineContent>
              <TimeCreated>
                {convertUTCToLocalString(item.created_at, "HH:mm DD/MM/YYYY")}
              </TimeCreated>
              <Note>{item?.note}</Note>
              <InfoCard
                avatar={item.userInfo?.avatar}
                name={item.userInfo?.name}
                description={item.userInfo?.email}
              />
            </CustomTimelineContent>
          </TimelineItem>
        );
      })}
    </CustomTimelineContainer>
  );
};

export default TimeLineAction;
