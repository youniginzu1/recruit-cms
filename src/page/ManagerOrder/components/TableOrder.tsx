import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";

import { Table } from "antd";
import type { ColumnsType } from "antd/es/table";

import { IOrderInfo } from "core/constants/interfaces";
import { getIndexTable } from "core/helpers";
import PaginationCustom from "core/components/PaginationCustom";
import { convertUTCToLocalString } from "core/helpers";
import { getColorStatusOrder, getTextStatusOrder } from "../helpers";
import { useAuth } from "auth/contexts/AuthProvider";

interface IProps {
  totalItems?: number;
  pageIndex: number;
  pageSize: number;
  data?: IOrderInfo[];
  isLoading?: boolean;
  onPageChange?: any;
  onRowsPerPageChange?: any;
}

const TableOrder = (props: IProps) => {
  const {
    totalItems = 0,
    pageIndex = 1,
    pageSize = 10,
    data,
    isLoading,
    onPageChange,
    onRowsPerPageChange,
  } = props;
  const { t } = useTranslation();
  const navigate = useNavigate();
  const { isHRPermission } = useAuth();

  const columns: ColumnsType<IOrderInfo> = [
    {
      title: t("table.index"),
      dataIndex: "index",
      key: "index",
      render: (value, record, index) => (
        <span>{getIndexTable(pageIndex, pageSize, index)}</span>
      ),
      width: "5%",
      align: "center",
    },
    {
      title: t("managerOrder.orderName"),
      dataIndex: "name",
    },
    {
      title: t("common.creator"),
      dataIndex: "name",
      render: (value, record, index) => <span>{record?.owner?.name}</span>,
      className: !isHRPermission() ? "d-none" : undefined,
    },
    {
      title: t("common.position"),
      dataIndex: "position",
      render: (value, record, index) => <span>{record?.position?.name}</span>,
    },
    {
      title: t("common.skill"),
      dataIndex: "skill",
      render: (value, record, index) => <span>{record?.skill?.name}</span>,
    },
    {
      title: t("common.level"),
      dataIndex: "level",
      render: (value, record, index) => <span>{record?.level?.name}</span>,
    },
    {
      title: t("common.status"),
      dataIndex: "status",
      render: (value, record, index) => (
        <span style={{ color: getColorStatusOrder(record?.status) }}>
          {getTextStatusOrder(record?.status)}
        </span>
      ),
    },
    {
      title: t("common.deadline"),
      dataIndex: "deadline",
      render: (value, record, index) => (
        <span>{convertUTCToLocalString(record?.deadline, "DD/MM/YYYY")}</span>
      ),
    },
  ];
  const rowSelection = {
    onChange: (selectedRowKeys: React.Key[], selectedRows: IOrderInfo[]) => {
      console.log(
        `selectedRowKeys: ${selectedRowKeys}`,
        "selectedRows: ",
        selectedRows
      );
    },
  };
  return (
    <>
      <Table
        rowSelection={{
          type: "checkbox",
          ...rowSelection,
        }}
        columns={columns}
        dataSource={data}
        pagination={false}
        loading={isLoading}
        bordered
        rowKey={(obj) => obj.id}
        className="tableCus table--rowSelect"
        onRow={(record, index) => ({
          onClick: () => {
            navigate(`detail-order/${record?.id}`);
          },
        })}
      />
      <PaginationCustom
        total={totalItems}
        pageIndex={pageIndex}
        pageSize={pageSize}
        onPageChange={onPageChange}
        onRowsPerPageChange={onRowsPerPageChange}
      />
    </>
  );
};

export default TableOrder;
