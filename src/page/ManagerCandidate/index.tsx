import TableCandidate from "./components/TableCandidate";
import StatisticsStatusCandidate from "./components/StatisticsStatusCandidate";
import FilterCandidate from "./components/FilterCandidate";
import { Grid } from "@mui/material";

import styles from "./styles.module.scss";
import { IFilter } from "core/constants/interfaces";
import useFilter from "core/hooks/common/useFilter";
import { useGetListCandidate } from "core/hooks/candidate/useGetListCandidate";

const defaultFilter: IFilter = {
  fullName: "",
  hrName: "",
  pageIndex: 1,
  pageSize: 10,
};

const ManagerCandidate = () => {
  const {
    filter,
    handleFilterChange,
    resetFilter,
    handlePageChange,
    handleRowsPerPageChange,
    handleSearch,
  } = useFilter(defaultFilter);

  const { data: uDataList, isLoading: isLoadingGetList } = useGetListCandidate({
    ...filter,
  });

  return (
    <Grid container spacing={2} className={styles.managerCandidateContainer}>
      <Grid item xs={12}>
        <StatisticsStatusCandidate
          data={uDataList?.metadata}
          totalItems={uDataList?.totalItems}
        />
      </Grid>
      <Grid container item xs={12} spacing={2}>
        <Grid item xs={9.5} className={styles.tableCandidateContainer}>
          <TableCandidate
            totalItems={uDataList?.totalItems}
            pageSize={uDataList?.pageSize}
            pageIndex={uDataList?.pageIndex}
            data={uDataList?.data}
            isLoading={isLoadingGetList}
            onPageChange={handlePageChange}
            onRowsPerPageChange={handleRowsPerPageChange}
          />
        </Grid>
        <Grid item xs={2.5}>
          <FilterCandidate
            handleSearch={handleSearch}
            resetFilter={resetFilter}
            filter={filter}
            handleFilterChange={handleFilterChange}
          />
        </Grid>
      </Grid>
    </Grid>
  );
};

export default ManagerCandidate;
