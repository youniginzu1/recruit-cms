import React from "react";
import { useTranslation } from "react-i18next";

import { Table } from "antd";
import type { ColumnsType } from "antd/es/table";

import { ICandidateInfo } from "core/constants/interfaces";
import { convertUTCToLocalString, getIndexTable } from "core/helpers";
import PaginationCustom from "core/components/PaginationCustom";
import { useNavigate } from "react-router-dom";
import {
  getColorStatusCandidate,
  getTextStatusCandidate,
} from "page/ManagerCandidate/helpers";

interface IProps {
  totalItems?: number;
  pageIndex: number;
  pageSize: number;
  data?: ICandidateInfo[];
  isLoading?: boolean;
  onPageChange?: any;
  onRowsPerPageChange?: any;
}

const TableCandidate = (props: IProps) => {
  const {
    totalItems = 0,
    pageIndex = 1,
    pageSize = 10,
    data,
    isLoading,
    onPageChange,
    onRowsPerPageChange,
  } = props;
  const { t } = useTranslation();
  const navigate = useNavigate();

  const columns: ColumnsType<ICandidateInfo> = [
    {
      title: t("table.index"),
      dataIndex: "index",
      key: "index",
      render: (value, record, index) => (
        <span>{getIndexTable(pageIndex, pageSize, index)}</span>
      ),
      width: "5%",
      align: "center",
    },
    {
      title: t("managerCandidate.candidateName"),
      dataIndex: "fullName",
    },
    {
      title: t("common.position"),
      dataIndex: "position",
      render: (value, record, index) => (
        <span>{record?.applyInfo?.position}</span>
      ),
    },
    {
      title: t("common.skill"),
      dataIndex: "skill",
      render: (value, record, index) => <span>{record?.applyInfo?.skill}</span>,
    },
    {
      title: t("common.level"),
      dataIndex: "level",
      render: (value, record, index) => <span>{record?.applyInfo?.level}</span>,
    },
    {
      title: t("common.status"),
      dataIndex: "status",
      render: (value, record, index) => (
        <span
          style={{ color: getColorStatusCandidate(record?.applyInfo?.status) }}
        >
          {t(
            getTextStatusCandidate(record?.applyInfo?.status) || "text.noneText"
          )}
        </span>
      ),
    },
    {
      title: t("managerCandidate.interviewDate"),
      dataIndex: "interviewDate",
      render: (value, record, index) => (
        <span>
          {convertUTCToLocalString(
            record?.applyInfo?.interviews?.[0]?.date,
            "DD/MM/YYYY"
          )}
        </span>
      ),
    },
    {
      title: t("managerCandidate.HRAssigned"),
      dataIndex: "hrAssigned",
      render: (value, record, index) => <span>{record?.hrAssigned?.name}</span>,
    },
  ];

  return (
    <>
      <Table
        columns={columns}
        dataSource={data}
        pagination={false}
        loading={isLoading}
        bordered
        rowKey={(obj) => obj.id}
        className="tableCus table--rowSelect"
        onRow={(record, index) => ({
          onClick: () => {
            navigate(`detail-candidate/${record?.id}`);
          },
        })}
      />
      <PaginationCustom
        total={totalItems}
        pageIndex={pageIndex}
        pageSize={pageSize}
        onPageChange={onPageChange}
        onRowsPerPageChange={onRowsPerPageChange}
      />
    </>
  );
};

export default TableCandidate;
