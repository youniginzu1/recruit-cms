import { useTranslation } from "react-i18next";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useMutation, useQueryClient } from "react-query";
import styled from "styled-components";
import { colors } from "core/assets";
import moment from "moment";

import { Grid } from "@mui/material";
import { Modal, message } from "antd";

import { apiCreateCandidate } from "core/api/candidate";
import { GenderType } from "core/constants/enums";
import { ICandidateForm } from "core/constants/interfaces";
import FooterModalSubmit from "core/components/FooterModalSubmit";
import DatePicker from "core/components/ReactHookForm/DatePicker";
import TextField from "core/components/ReactHookForm/TextField";
import RadioGroup from "core/components/ReactHookForm/RadioGroup";
import Autocomplete from "core/components/ReactHookForm/Autocomplete";
import { useAuth } from "auth/contexts/AuthProvider";
import { USE_GET_LIST_CANDIDATE } from "core/constants/queryName";

interface IProps {
  open: boolean;
  toggle: () => void;
}

const TitleInfo = styled.span`
  font-weight: 700;
  font-size: 16px;
  line-height: 19px;
  color: ${colors.mainColor};
`;

const ModalAddCandidate = (props: IProps) => {
  const { open, toggle } = props;
  const { t } = useTranslation();
  const queryClient = useQueryClient();
  const { listPosition, listSkill, listLevel, listSchool, listExperience } =
    useAuth();

  const {
    handleSubmit,
    control,
    setValue,
    reset,
    clearErrors,
    formState: { errors },
  } = useForm<ICandidateForm>({
    resolver: yupResolver(
      yup
        .object({
          fullName: yup
            .string()
            .nullable()
            .required(t("validate.required") as string)
            .min(6, t("validate.nameMin6") as string),
          birthday: yup
            .string()
            .required(t("validate.required") as string)
            .nullable(),
          email: yup
            .string()
            .email(t("validate.emailValid") as string)
            .required(t("validate.required") as string),
          mobile: yup
            .string()
            .nullable()
            .required(t("validate.required") as string)
            .matches(
              /^(84|0[3|5|7|8|9])+([0-9]{8})$/,
              t("validate.phoneInvalid") as string
            ),
          school: yup
            .string()
            .required(t("validate.required") as string)
            .nullable(),
          positionId: yup
            .string()
            .required(t("validate.required") as string)
            .nullable(),
          skillId: yup
            .string()
            .required(t("validate.required") as string)
            .nullable(),
          levelId: yup
            .string()
            .required(t("validate.required") as string)
            .nullable(),
          experience: yup
            .string()
            .required(t("validate.required") as string)
            .nullable(),
          applyDate: yup
            .string()
            .required(t("validate.required") as string)
            .nullable(),
          linkCvOnline: yup
            .string()
            .required(t("validate.required") as string)
            .nullable(),
        })
        .required()
    ),
    mode: "onChange",
  });

  const listRadioGender = [
    {
      label: t("common.male"),
      value: GenderType.MALE,
    },
    {
      label: t("common.female"),
      value: GenderType.FEMALE,
    },
  ];

  const { mutate: createCandidate, isLoading: isLoadingSaveCandidate } =
    useMutation((params: any) => apiCreateCandidate(params), {
      onSuccess: () => {
        queryClient.invalidateQueries(USE_GET_LIST_CANDIDATE);
        message.success(t("message.addCandidateSuccess"));
        toggle();
      },
    });

  return (
    <Modal
      title={t("modal.addCandidateTitle")}
      open={open}
      className="modal-custom"
      centered
      onCancel={toggle}
      footer={
        <FooterModalSubmit
          onOk={handleSubmit((values) => createCandidate(values))}
          onCancel={toggle}
          isLoadingOnOk={isLoadingSaveCandidate}
        />
      }
    >
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <TitleInfo>{t("form.basicInfoTitle")}</TitleInfo>
        </Grid>
        <Grid item xs={12}>
          <TextField
            name="fullName"
            label={t("managerCandidate.candidateName")}
            errors={errors}
            control={control}
            required
          />
        </Grid>
        <Grid item xs={12}>
          <DatePicker
            name="birthday"
            label={t("common.birthday")}
            control={control}
            errors={errors}
            maxDate={moment()}
            required
          />
        </Grid>
        <Grid item xs={12}>
          <RadioGroup
            name="sex"
            label={t("common.gender")}
            control={control}
            radioList={listRadioGender}
            defaultValue={GenderType.MALE}
            isRowDirection
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            name="mobile"
            label={t("common.phoneNumber")}
            errors={errors}
            control={control}
            required
            maxLength={11}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            name="email"
            label={t("common.email")}
            errors={errors}
            control={control}
            required
          />
        </Grid>
        <Grid item xs={12}>
          <Autocomplete
            name="school"
            label={t("common.school")}
            control={control}
            errors={errors}
            clearErrors={clearErrors}
            listOptions={listSchool}
            getOptionLabel={(option) => option.name}
            customRenderOption={(option) => <span>{option.name}</span>}
            onChange={(option) => setValue("school", option?.id ?? null)}
            required
          />
        </Grid>
        <Grid item xs={12}>
          <TitleInfo>{t("form.cvInfoTitle")}</TitleInfo>
        </Grid>
        <Grid item xs={6}>
          <Autocomplete
            name="positionId"
            label={t("common.position")}
            control={control}
            errors={errors}
            clearErrors={clearErrors}
            listOptions={listPosition}
            getOptionLabel={(option) => option.name}
            customRenderOption={(option) => <span>{option.name}</span>}
            onChange={(option) => setValue("positionId", option?.id ?? null)}
            required
          />
        </Grid>
        <Grid item xs={6}>
          <Autocomplete
            name="skillId"
            label={t("common.skill")}
            control={control}
            errors={errors}
            clearErrors={clearErrors}
            listOptions={listSkill}
            getOptionLabel={(option) => option.name}
            customRenderOption={(option) => <span>{option.name}</span>}
            onChange={(option) => setValue("skillId", option?.id ?? null)}
            required
          />
        </Grid>
        <Grid item xs={6}>
          <Autocomplete
            name="levelId"
            label={t("common.level")}
            control={control}
            errors={errors}
            clearErrors={clearErrors}
            listOptions={listLevel}
            getOptionLabel={(option) => option.name}
            customRenderOption={(option) => <span>{option.name}</span>}
            onChange={(option) => setValue("levelId", option?.id ?? null)}
            required
          />
        </Grid>
        <Grid item xs={6}>
          <Autocomplete
            name="experience"
            label={t("common.experience")}
            control={control}
            errors={errors}
            clearErrors={clearErrors}
            listOptions={listExperience}
            getOptionLabel={(option) => option.name}
            customRenderOption={(option) => <span>{option.name}</span>}
            onChange={(option) => setValue("experience", option?.id ?? null)}
            required
          />
        </Grid>
        <Grid item xs={12}>
          <DatePicker
            name="applyDate"
            label={t("managerCandidate.applyDate")}
            control={control}
            errors={errors}
            maxDate={moment()}
            required
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            name="linkCvOnline"
            label={t("managerCandidate.linkCvOnline")}
            errors={errors}
            control={control}
            required
          />
        </Grid>
      </Grid>
    </Modal>
  );
};

export default ModalAddCandidate;
