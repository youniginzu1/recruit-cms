import { Grid } from "@mui/material";
import styles from "./styles.module.scss";

interface IProps {
  data: any;
  totalItems: any;
}
const StatisticsStatusCandidate = (props: IProps) => {
  const { data, totalItems } = props;
  return (
    <Grid container gap={1} className={styles.statisticsStatusContainer}>
      <Grid item xs={1.7} className={styles.totalBlock}>
        <span className={styles.title}>Tổng số UV</span>
        <span className={styles.total}>{totalItems}</span>
      </Grid>
      <Grid item xs={2.4} className={styles.waitBlock}>
        <Grid item xs={12}>
          <span className={styles.title}>Qua sơ tuyển</span>
          <span className={styles.total}>
            {" "}
            ({data?.totalPassPreQualification})
          </span>
        </Grid>
        <Grid item xs={12}>
          <span className={styles.title}>Chờ leader duyệt CV</span>
          <span className={styles.total}>
            {" "}
            ({data?.totalWaitingLeaderApproveCV})
          </span>
        </Grid>
        <Grid item xs={12}>
          <span className={styles.title}>Chờ leader duyệt UV</span>
          <span className={styles.total}>
            {" "}
            ({data?.totalWaitingLeaderApproveCandidate})
          </span>
        </Grid>
      </Grid>
      <Grid item xs={2.4} className={styles.waitBlock}>
        <Grid item xs={12}>
          <span className={styles.title}>Chờ UV phản hồi lời mời</span>
          <span className={styles.total}>
            {" "}
            ({data?.totalWaitingCandidateRespondInvitation})
          </span>
        </Grid>
        <Grid item xs={12}>
          <span className={styles.title}>Chờ UV phản hồi Offer</span>
          <span className={styles.total}>
            {" "}
            ({data?.totalWaitingCandidateRespondOffer})
          </span>
        </Grid>
      </Grid>
      <Grid item xs={2.5} className={styles.onBoardBlock}>
        <Grid item xs={12}>
          <span className={styles.title}>Đang chờ phỏng vấn</span>
          <span className={styles.total}> ({data?.totalWaitingInterview})</span>
        </Grid>
        <Grid item xs={12}>
          <span className={styles.title}>Onboard</span>
          <span className={styles.total}> ({data?.totalOnboard})</span>
        </Grid>
      </Grid>
      <Grid item xs={2.6} className={styles.endInterviewBlock}>
        <Grid item xs={12}>
          <span className={styles.title}>Từ chối Offer</span>
          <span className={styles.total}>
            {" "}
            ({data?.totalCandidateRefusedOffer})
          </span>
        </Grid>
        <Grid item xs={12}>
          <span className={styles.title}>Leader không duyệt CV</span>
          <span className={styles.total}>
            {" "}
            ({data?.totalLeaderNotApproveCV})
          </span>
        </Grid>
        <Grid item xs={12}>
          <span className={styles.title}>Leader không duyệt UV</span>
          <span className={styles.total}>
            {" "}
            ({data?.totalLeaderNotApproveCandidate})
          </span>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default StatisticsStatusCandidate;
