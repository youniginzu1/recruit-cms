import { Modal } from "antd";
import { useState } from "react";
import { useTranslation } from "react-i18next";
import { useMutation, useQueryClient } from "react-query";
import { useForm, useWatch } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import styled from "styled-components";
import { colors } from "core/assets";

import { Grid } from "@mui/material";
import { TimePicker } from "antd";

import { createInterviewSchedule } from "core/api/candidate";
import FooterModalSubmit from "core/components/FooterModalSubmit";
import CustomTextField from "core/components/ReactHookForm/TextField";
import DatePicker from "core/components/ReactHookForm/DatePicker";
import moment from "moment";
import CustomRadioGroup from "core/components/ReactHookForm/RadioGroup";
import { convertUTCToLocalString } from "core/helpers";

const TitleInfo = styled.span`
  font-weight: 700;
  font-size: 16px;
  line-height: 19px;
  color: ${colors.mainColor};
`;

interface IProps {
  open: boolean;
  onCancel: () => void;
  candidateId?: string;
}

const ModalBookInterview = (props: IProps) => {
  const { open, onCancel, candidateId } = props;
  const { t } = useTranslation();
  const queryClient = useQueryClient();
  const [timeString, setTimeString] = useState<any>([]);

  const {
    handleSubmit,
    control,
    formState: { errors },
  } = useForm<any>({
    resolver: yupResolver(
      yup.object({
        interviewTitle: yup
          .string()
          .nullable()
          .required(t("validate.required") as string)
          .min(6, t("validate.nameMin6") as string),
      })
    ),
    mode: "onChange",
  });

  const { mutate: bookingInterview, isLoading } = useMutation(
    (params: any) =>
      createInterviewSchedule({
        ...params,
        interviewStartTime: timeString[0],
        interviewEndTime: timeString[1],
        interviewDate: convertUTCToLocalString(
          params.interviewDate.$d,
          "YYYY/MM/DD"
        ),
        candidateId,
      }),
    {
      onSuccess: () => {
        queryClient.invalidateQueries(["detailCandidate"]);
        onCancel();
      },
    }
  );

  const listRadioTypeInterview = [
    {
      label: "Offline",
      value: 1,
    },
    {
      label: "Online",
      value: 2,
    },
  ];

  const interviewTypeWatch = useWatch({
    control,
    name: "interviewType",
  });

  return (
    <Modal
      title={t("modal.createInterviewSchedule")}
      open={open}
      onCancel={onCancel}
      className="modal-custom"
      centered
      footer={
        <FooterModalSubmit
          onOk={handleSubmit((values) => bookingInterview(values))}
          isLoadingOnOk={isLoading}
        />
      }
    >
      <Grid container rowSpacing={2}>
        <Grid item xs={12}>
          <TitleInfo>Thông tin lịch PV</TitleInfo>
        </Grid>
        <Grid item xs={12}>
          <CustomTextField
            name="interviewTitle"
            label="Tiêu đề"
            control={control}
            errors={errors}
            required
          />
        </Grid>
        <Grid item xs={12}>
          <DatePicker
            name="interviewDate"
            label={t("managerCandidate.interviewDate")}
            control={control}
            errors={errors}
            required
          />
        </Grid>
        <Grid item xs={12}>
          <TimePicker.RangePicker
            format="HH:mm"
            onChange={(time, timeString) => setTimeString(timeString)}
          />
        </Grid>
        <Grid item xs={12}>
          <TitleInfo>Hình thức PV</TitleInfo>
        </Grid>
        <Grid item xs={12}>
          <CustomRadioGroup
            name="interviewType"
            control={control}
            radioList={listRadioTypeInterview}
            defaultValue={1}
            isRowDirection
          />
        </Grid>
        {interviewTypeWatch === "2" && (
          <Grid item xs={12}>
            <CustomTextField
              name="linkInterviewOnline"
              label="Link PV online"
              errors={errors}
              control={control}
            />
          </Grid>
        )}
        <Grid item xs={12}>
          <CustomTextField
            name="description"
            label="Mô tả"
            errors={errors}
            control={control}
            rows={4}
          />
        </Grid>
      </Grid>
    </Modal>
  );
};

export default ModalBookInterview;
