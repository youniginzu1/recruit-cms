import { useTranslation } from "react-i18next";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useMutation, useQueryClient } from "react-query";
import styled from "styled-components";
import { colors } from "core/assets";
import moment from "moment";

import { Grid } from "@mui/material";
import { Modal, message } from "antd";

import { apiCreateCandidate, putCandidate } from "core/api/candidate";
import { GenderType } from "core/constants/enums";
import { ICandidateForm } from "core/constants/interfaces";
import FooterModalSubmit from "core/components/FooterModalSubmit";
import DatePicker from "core/components/ReactHookForm/DatePicker";
import TextField from "core/components/ReactHookForm/TextField";
import RadioGroup from "core/components/ReactHookForm/RadioGroup";
import Autocomplete from "core/components/ReactHookForm/Autocomplete";
import { useAuth } from "auth/contexts/AuthProvider";
import { USE_GET_LIST_CANDIDATE } from "core/constants/queryName";

interface IProps {
  open: boolean;
  toggle: () => void;
  data: any;
}

const TitleInfo = styled.span`
  font-weight: 700;
  font-size: 16px;
  line-height: 19px;
  color: ${colors.mainColor};
`;

export default function ApplyInfoModal(props: IProps) {
  const { open, toggle, data } = props;
  const { t } = useTranslation();
  const queryClient = useQueryClient();
  const { listPosition, listSkill, listLevel, listSchool, listExperience } =
    useAuth();

  const {
    handleSubmit,
    control,
    setValue,
    reset,
    clearErrors,
    formState: { errors },
  } = useForm<ICandidateForm>({
    resolver: yupResolver(
      yup
        .object({
          positionId: yup
            .string()
            .required(t("validate.required") as string)
            .nullable(),
          skillId: yup
            .string()
            .required(t("validate.required") as string)
            .nullable(),
          levelId: yup
            .string()
            .required(t("validate.required") as string)
            .nullable(),
          experience: yup
            .string()
            .required(t("validate.required") as string)
            .nullable(),
          applyDate: yup
            .string()
            .required(t("validate.required") as string)
            .nullable(),
        })
        .required()
    ),
    mode: "onChange",
    defaultValues: {
      positionId: data?.applyInfo?.position?.id,
      skillId: data?.applyInfo?.skill?.id,
      levelId: data?.applyInfo?.level?.id,
      experience: data?.applyInfo?.experience?.id,
    },
  });

  const { mutate: updateCandidate, isLoading: isLoadingUpdateCandidate } =
    useMutation((payload: any) => putCandidate(payload), {
      onSuccess: () => {
        queryClient.invalidateQueries("detailCandidate");
        message.success(t("message.updateApplyInfoSuccess"));
        toggle();
      },
    });

  return (
    <Modal
      title={t("modal.updateApplyInfoLabel")}
      open={open}
      className="modal-custom"
      centered
      onCancel={toggle}
      footer={
        <FooterModalSubmit
          onOk={handleSubmit((values) =>
            updateCandidate({ ...values, id: data?.id })
          )}
          onCancel={toggle}
          isLoadingOnOk={isLoadingUpdateCandidate}
        />
      }
    >
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <TitleInfo>{t("form.cvInfoTitle")}</TitleInfo>
        </Grid>
        <Grid item xs={6}>
          <Autocomplete
            name="positionId"
            label={t("common.position")}
            control={control}
            errors={errors}
            clearErrors={clearErrors}
            listOptions={listPosition}
            getOptionLabel={(option) => option.name}
            customRenderOption={(option) => <span>{option.name}</span>}
            onChange={(option) => setValue("positionId", option?.id ?? null)}
            required
            defaultValue={data?.applyInfo?.position}
          />
        </Grid>
        <Grid item xs={6}>
          <Autocomplete
            name="skillId"
            label={t("common.skill")}
            control={control}
            errors={errors}
            clearErrors={clearErrors}
            listOptions={listSkill}
            getOptionLabel={(option) => option.name}
            customRenderOption={(option) => <span>{option.name}</span>}
            onChange={(option) => setValue("skillId", option?.id ?? null)}
            required
            defaultValue={data?.applyInfo?.skill}
          />
        </Grid>
        <Grid item xs={6}>
          <Autocomplete
            name="levelId"
            label={t("common.level")}
            control={control}
            errors={errors}
            clearErrors={clearErrors}
            listOptions={listLevel}
            getOptionLabel={(option) => option.name}
            customRenderOption={(option) => <span>{option.name}</span>}
            onChange={(option) => setValue("levelId", option?.id ?? null)}
            required
            defaultValue={data?.applyInfo?.level}
          />
        </Grid>
        <Grid item xs={6}>
          <Autocomplete
            name="experience"
            label={t("common.experience")}
            control={control}
            errors={errors}
            clearErrors={clearErrors}
            listOptions={listExperience}
            getOptionLabel={(option) => option.name}
            customRenderOption={(option) => <span>{option.name}</span>}
            onChange={(option) => setValue("experience", option?.id ?? null)}
            required
            defaultValue={data?.applyInfo?.experience}
          />
        </Grid>
        <Grid item xs={12}>
          <DatePicker
            name="applyDate"
            label={t("managerCandidate.applyDate")}
            control={control}
            errors={errors}
            maxDate={moment()}
            required
            defaultValue={data?.applyInfo?.applyDate}
          />
        </Grid>
      </Grid>
    </Modal>
  );
}
