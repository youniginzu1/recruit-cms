import { Modal } from "antd";
import { useTranslation } from "react-i18next";
import { useMutation, useQueryClient } from "react-query";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";

import { Grid } from "@mui/material";

import { offerCandidate } from "core/api/candidate";
import FooterModalSubmit from "core/components/FooterModalSubmit";
import CustomTextField from "core/components/ReactHookForm/TextField";
import CustomAutocomplete from "core/components/ReactHookForm/Autocomplete";
import { useAuth } from "auth/contexts/AuthProvider";
import DatePicker from "core/components/ReactHookForm/DatePicker";
import { convertUTCToLocalString } from "core/helpers";

interface IProps {
  open: boolean;
  onCancel: () => void;
  candidateId?: string;
}

const ModalBookInterview = (props: IProps) => {
  const { open, onCancel, candidateId } = props;
  const { t } = useTranslation();
  const queryClient = useQueryClient();
  const { listPersonnelType, listContractType } = useAuth();

  const {
    handleSubmit,
    control,
    clearErrors,
    setValue,
    formState: { errors },
  } = useForm<any>({
    resolver: yupResolver(yup.object({})),
    mode: "onChange",
  });

  const { mutate: offerSubmit, isLoading } = useMutation(
    (params: any) =>
      offerCandidate({
        ...params,
        workDate: convertUTCToLocalString(params.workDate.$d, "YYYY/MM/DD"),
        candidateId,
      }),
    {
      onSuccess: () => {
        queryClient.invalidateQueries(["detailCandidate"]);
        onCancel();
      },
    }
  );
  return (
    <Modal
      title={t("modal.offerCandidate")}
      open={open}
      onCancel={onCancel}
      className="modal-custom"
      footer={
        <FooterModalSubmit
          onOk={handleSubmit((values) => offerSubmit(values))}
          isLoadingOnOk={isLoading}
        />
      }
    >
      <Grid container rowSpacing={2}>
        <Grid item xs={12}>
          <CustomTextField
            name="salary"
            label="Mức lương"
            control={control}
            errors={errors}
            required
            type="number"
          />
        </Grid>
        <Grid item xs={12}>
          <CustomAutocomplete
            name="personnelTypeId"
            label="Loại nhân sự"
            control={control}
            errors={errors}
            clearErrors={clearErrors}
            listOptions={listPersonnelType}
            getOptionLabel={(option) => option.name}
            customRenderOption={(option) => <span>{option.name}</span>}
            onChange={(option) =>
              setValue("personnelTypeId", option?.id ?? null)
            }
            required
          />
        </Grid>
        <Grid item xs={12}>
          <CustomAutocomplete
            name="contractTypeId"
            label="Loại hợp đồng"
            control={control}
            errors={errors}
            clearErrors={clearErrors}
            listOptions={listContractType}
            getOptionLabel={(option) => option.name}
            customRenderOption={(option) => <span>{option.name}</span>}
            onChange={(option) =>
              setValue("contractTypeId", option?.id ?? null)
            }
            required
          />
        </Grid>
        <Grid item xs={12}>
          <DatePicker
            name="workDate"
            label={t("text.onBoardDate")}
            control={control}
            errors={errors}
            required
          />
        </Grid>
      </Grid>
    </Modal>
  );
};

export default ModalBookInterview;
