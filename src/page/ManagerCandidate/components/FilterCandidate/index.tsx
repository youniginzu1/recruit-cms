import { useState } from "react";
import { useTranslation } from "react-i18next";

import { Button, DatePicker, Input } from "antd";
import { Grid } from "@mui/material";

import styles from "./styles.module.scss";
import { images } from "core/assets";
import ModalAddCandidate from "../ModalAddCandidate";
import InputFilterByModal from "core/components/InputFilterByModal";
import { useAuth } from "auth/contexts/AuthProvider";
import { ListStatusCandidate } from "core/constants/status";
import { convertUTCToLocalString } from "core/helpers";

interface IProps {
  filter?: any;
  handleSearch?: any;
  resetFilter?: any;
  handleFilterChange?: any;
}

const FilterCandidate = (props: IProps) => {
  const { t } = useTranslation();
  const { handleSearch, filter, resetFilter, handleFilterChange } = props;
  const { listPosition, listSkill, listLevel } = useAuth();
  const [openAddCandidate, setOpenAddCandidate] = useState(false);

  const listStatus = ListStatusCandidate.map((item) => {
    return { ...item, name: t(item.name), id: item.status };
  });

  return (
    <Grid container className={styles.filterContainer}>
      <Grid item xs={12} pb={1.5}>
        <Button
          className="btn w-100"
          icon={<img src={images.addIcon} alt="" />}
          onClick={() => {
            setOpenAddCandidate(true);
          }}
        >
          {t("btn.addCandidate")}
        </Button>
      </Grid>
      <Grid item xs={12} pb={1.5}>
        <Input
          placeholder={t("placeholder.candidateName")}
          prefix={<img src={images.searchIcon} />}
          className="inputCustom fs-12"
          onChange={(text) => handleSearch.keywordSearch(text, "fullName")}
        />
      </Grid>
      <Grid item xs={12} pb={1.5}>
        <Input
          placeholder={t("placeholder.hrName")}
          prefix={<img src={images.searchIcon} />}
          className="inputCustom fs-12"
          onChange={(text) => handleSearch.keywordSearch(text, "hrName")}
        />
      </Grid>
      <Grid item xs={12} pb={1.5}>
        <InputFilterByModal
          titleModal={t("modal.filterByPosition")}
          placeholderInput={t("common.position")}
          listFilter={listPosition}
          itemIdSelected={filter?.positionId}
          setItemIdSelected={(id: number) =>
            handleFilterChange({ positionId: id })
          }
        />
      </Grid>
      <Grid item xs={12} pb={1.5}>
        <InputFilterByModal
          titleModal={t("modal.filterBySkill")}
          placeholderInput={t("common.skill")}
          listFilter={listSkill}
          itemIdSelected={filter?.skillId}
          setItemIdSelected={(id: number) =>
            handleFilterChange({ skillId: id })
          }
        />
      </Grid>
      <Grid item xs={12} pb={1.5}>
        <InputFilterByModal
          titleModal={t("modal.filterByLevel")}
          placeholderInput={t("common.level")}
          listFilter={listLevel}
          itemIdSelected={filter?.levelId}
          setItemIdSelected={(id: number) =>
            handleFilterChange({ levelId: id })
          }
        />
      </Grid>
      <Grid item xs={12} pb={1.5}>
        <InputFilterByModal
          titleModal={t("modal.filterByStatus")}
          placeholderInput={t("common.status")}
          listFilter={listStatus}
          itemIdSelected={filter?.status}
          setItemIdSelected={(id: number) => handleFilterChange({ status: id })}
        />
      </Grid>
      <Grid item xs={12} pb={1.5}>
        <Button
          className="btn btnForm btn--reset"
          icon={<img src={images.refreshIcon} />}
          onClick={() => resetFilter()}
        >
          {t("btn.reset")}
        </Button>
      </Grid>
      {openAddCandidate && (
        <ModalAddCandidate
          open={openAddCandidate}
          toggle={() => setOpenAddCandidate(false)}
        />
      )}
    </Grid>
  );
};

export default FilterCandidate;
