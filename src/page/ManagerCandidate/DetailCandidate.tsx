import { useTranslation } from "react-i18next";
import { useQuery, useMutation, useQueryClient } from "react-query";
import { useParams } from "react-router-dom";
import { useState } from "react";

import { Divider, Button, Popconfirm } from "antd";

import styles from "./styles.module.scss";
import { images } from "core/assets";
import LabelInfo from "core/components/LabelInfo";
import BackHeader from "core/components/BackHeader";
import { convertUTCToLocalString, formatSalary } from "core/helpers";
import { getDetailCandidate } from "core/api/candidate";
import useTransformIdToText from "core/hooks/common/useTransformIdToText";
import CandidateInfoModal from "./components/CandidateInfoModal";
import ApplyInfoModal from "./components/ApplyInfoModal";
import ModalBookInterview from "./components/ModalBookInterview";
import ModalOffer from "./components/ModalOffer";
import {
  arrHRPermission,
  arrLeaderPermission,
  getColorStatusCandidate,
  getNextStatusAccept,
  getNextStatusDeny,
  getTextButtonAccept,
  getTextButtonDeny,
  getTextStatusCandidate,
} from "./helpers";
import { updateStatusCandidate } from "core/api/candidate";
import { useAuth } from "auth/contexts/AuthProvider";
import { getValidHttpsUrl } from "../../core/helpers";
import { ListIdStatusCandidate } from "core/constants/status";

const DetailCandidate = () => {
  const [t] = useTranslation();
  const queryClient = useQueryClient();
  const { id } = useParams();
  const { userInfo } = useAuth();
  const { transformGender } = useTransformIdToText();

  const [action, setAction] = useState<any>({ deny: false, accept: false });
  const [openConfirm, setOpenConfirm] = useState(false);
  const [openCandidateInfoModal, setOpenCandidateInfoModal] = useState(false);
  const [openApplyInfoModal, setOpenApplyInfoModal] = useState(false);
  const [openBookingModal, setOpenBookingModal] = useState(false);
  const [openOfferModal, setOpenOfferModal] = useState(false);

  const { data: detailCandidate } = useQuery(["detailCandidate", id], () =>
    getDetailCandidate(id)
  );

  const { mutate: updateStatus, isLoading: isLoadingUpdateStatus } =
    useMutation(
      (params: any) => updateStatusCandidate({ ...params, candidateId: id }),
      {
        onSuccess: () => {
          queryClient.invalidateQueries(["detailCandidate"]);
        },
      }
    );

  return (
    <div className={styles.detailCandidateContainer}>
      <div className="d-flex-sb-c">
        <BackHeader title={t("text.detail")} />
        <div className="d-flex">
          {getTextButtonDeny(
            detailCandidate?.data?.applyInfo?.status,
            userInfo?.role?.id
          ) && (
            <Popconfirm
              title={t("alert.areYouSure")}
              open={action?.deny && openConfirm}
              onConfirm={() =>
                updateStatus({
                  status: getNextStatusDeny(
                    detailCandidate?.data?.applyInfo?.status
                  ),
                })
              }
              onCancel={() => setOpenConfirm(false)}
              okButtonProps={{ loading: action?.deny && isLoadingUpdateStatus }}
            >
              <Button
                className="btn btn--deny mr-10"
                onClick={() => {
                  setAction({ deny: true });
                  setOpenConfirm(true);
                }}
              >
                {getTextButtonDeny(
                  detailCandidate?.data?.applyInfo?.status,
                  userInfo?.role?.id
                )}
              </Button>
            </Popconfirm>
          )}

          {getTextButtonAccept(
            detailCandidate?.data?.applyInfo?.status,
            userInfo?.role?.id
          ) && (
            <Popconfirm
              title={t("alert.areYouSure")}
              open={action?.accept && openConfirm}
              onConfirm={() =>
                updateStatus({
                  status: getNextStatusAccept(
                    detailCandidate?.data?.applyInfo?.status
                  ),
                })
              }
              onCancel={() => setOpenConfirm(false)}
              okButtonProps={{
                loading: action?.accept && isLoadingUpdateStatus,
              }}
            >
              <Button
                className="btn btn--accept"
                onClick={() => {
                  setAction({ accept: true });
                  setOpenConfirm(true);
                }}
              >
                {getTextButtonAccept(
                  detailCandidate?.data?.applyInfo?.status,
                  userInfo?.role?.id
                )}
              </Button>
            </Popconfirm>
          )}
          {detailCandidate?.data?.applyInfo?.status ===
            ListIdStatusCandidate.WAIT_CANDIDATE_RESPOND_INVITATION &&
            arrHRPermission.includes(userInfo?.role?.id) && (
              <Button
                onClick={() => setOpenBookingModal(true)}
                className="btn btn--accept"
              >
                {t("btn.bookInterview")}
              </Button>
            )}

          {detailCandidate?.data?.applyInfo?.status ===
            ListIdStatusCandidate.WAIT_LEADER_APPROVE_CANDIDATE &&
            arrLeaderPermission.includes(userInfo?.role?.id) && (
              <Button
                onClick={() => setOpenOfferModal(true)}
                className="btn btn--accept"
              >
                {t("btn.offer")}
              </Button>
            )}
        </div>
      </div>

      <div className="mt-35">
        <LabelInfo title={t("label.infoCandidate")} />
        <div className={styles.infoContainer}>
          <div
            className={styles.editIcon}
            onClick={() => setOpenCandidateInfoModal(true)}
          >
            <img src={images.editIcon} />
          </div>
          <div className={styles.infoData}>
            <div className="text-left mw-170">{t("text.name")}</div>
            <div className="text-2">
              {detailCandidate?.data?.fullName || t("text.noneText")}
            </div>
          </div>
          <div className={styles.infoData}>
            <div className="text-left mw-170">{t("text.email")}</div>
            <div className="text-2">
              {detailCandidate?.data?.email || t("text.noneText")}
            </div>
          </div>
          <div className={styles.infoData}>
            <div className="text-left mw-170">{t("text.birthday")}</div>
            <div className="text-2">
              {convertUTCToLocalString(
                detailCandidate?.data?.birthday,
                "DD/MM/YYYY"
              ) || t("text.noneText")}
            </div>
          </div>
          <div className={styles.infoData}>
            <div className="text-left mw-170">{t("text.phone")}</div>
            <div className="text-2">
              {detailCandidate?.data?.mobile || t("text.noneText")}
            </div>
          </div>
          <div className={styles.infoData}>
            <div className="text-left mw-170">{t("text.sex")}</div>
            <div className="text-2">
              {transformGender(detailCandidate?.data?.sex) ||
                t("text.noneText")}
            </div>
          </div>
          <div className={styles.infoData}>
            <div className="text-left mw-170">{t("text.school")}</div>
            <div className="text-2">
              {detailCandidate?.data?.applyInfo?.school?.name ||
                t("text.noneText")}
            </div>
          </div>
        </div>
      </div>
      <div className="mt-35">
        <LabelInfo title={t("label.infoApply")} />
        <div className={styles.infoContainer}>
          <div
            className={styles.editIcon}
            onClick={() => setOpenApplyInfoModal(true)}
          >
            <img src={images.editIcon} />
          </div>
          <div className={styles.infoDataApply}>
            <div className="text-left text-left--size-small mw-170">
              {t("managerOrder.jobIncludes")}
            </div>
            <div className="text-2 text-2--size-small">
              {detailCandidate?.data?.applyInfo?.job?.name ||
                t("text.noneText")}
            </div>
          </div>
          <div className={styles.infoDataApply}>
            <div className="text-left text-left--size-small mw-170">
              {t("text.applyDate")}
            </div>
            <div className="text-2 text-2--size-small">
              {convertUTCToLocalString(
                detailCandidate?.data?.applyInfo?.applyDate,
                "DD/MM/YYYY"
              ) || t("text.noneText")}
            </div>
          </div>
          <div className={styles.infoDataApply}>
            <div className="text-left text-left--size-small mw-170">
              {t("text.interviewDate")}
            </div>
            <div className="text-2 text-2--size-small">
              {convertUTCToLocalString(
                detailCandidate?.data?.applyInfo?.interviews?.[0]?.date,
                "DD/MM/YYYY"
              ) || t("text.noneText")}
            </div>
          </div>
          <div className={styles.infoDataApply}>
            <div className="text-left text-left--size-small mw-170">
              {t("text.HRAssigned")}
            </div>
            <div className="text-2 text-2--size-small">
              {detailCandidate?.data?.hrAssigned?.name || t("text.noneText")}
            </div>
          </div>
          <div className={styles.infoDataApply}>
            <div className="text-left text-left--size-small mw-170">
              {t("text.statusApply")}
            </div>
            <div
              className="text-2 text-2--size-small"
              style={{
                color: getColorStatusCandidate(
                  detailCandidate?.data?.applyInfo?.status
                ),
              }}
            >
              {t(
                getTextStatusCandidate(
                  detailCandidate?.data?.applyInfo?.status
                ) || "text.noneText"
              )}
            </div>
          </div>
          <Divider style={{ margin: "10px 0px", borderTopWidth: 2 }} />
          <div className={styles.infoDataApply}>
            <div className="text-left text-left--size-small mw-170">
              {t("text.position")}
            </div>
            <div className="text-2 text-2--size-small">
              {detailCandidate?.data?.applyInfo?.position?.name ||
                t("text.noneText")}
            </div>
          </div>
          <div className={styles.infoDataApply}>
            <div className="text-left text-left--size-small mw-170">
              {t("common.level")}
            </div>
            <div className="text-2 text-2--size-small">
              {detailCandidate?.data?.applyInfo?.level?.name ||
                t("text.noneText")}
            </div>
          </div>
          <div className={styles.infoDataApply}>
            <div className="text-left text-left--size-small mw-170">
              {t("text.skill")}
            </div>
            <div className="text-2 text-2--size-small">
              {detailCandidate?.data?.applyInfo?.skill?.name ||
                t("text.noneText")}
            </div>
          </div>
          <div className={styles.infoDataApply}>
            <div className="text-left text-left--size-small mw-170">
              {t("text.experience")}
            </div>
            <div className="text-2 text-2--size-small">
              {detailCandidate?.data?.applyInfo?.experience?.name ||
                t("text.noneText")}
            </div>
          </div>
          <div className={styles.infoDataApply}>
            <div className="text-left text-left--size-small mw-170">
              {t("text.employeeType")}
            </div>
            <div className="text-2 text-2--size-small">
              {detailCandidate?.data?.applyInfo?.offers?.[0]?.personnelType
                ?.name || t("text.noneText")}
            </div>
          </div>
          <div className={styles.infoDataApply}>
            <div className="text-left text-left--size-small mw-170">
              {t("text.contractType")}
            </div>
            <div className="text-2 text-2--size-small">
              {detailCandidate?.data?.applyInfo?.offers?.[0]?.contractType
                ?.name || t("text.noneText")}
            </div>
          </div>
          <div className={styles.infoDataApply}>
            <div className="text-left text-left--size-small mw-170">
              {t("text.salary")}
            </div>
            <div className="text-2 text-2--size-small">
              {formatSalary(
                detailCandidate?.data?.applyInfo?.offers?.[0]?.salary
              ) || t("text.noneText")}
            </div>
          </div>
          <div className={styles.infoDataApply}>
            <div className="text-left text-left--size-small mw-170">
              {t("text.onBoardDate")}
            </div>
            <div className="text-2 text-2--size-small">
              {convertUTCToLocalString(
                detailCandidate?.data?.applyInfo?.offers?.[0]?.workDate,
                "DD/MM/YYYY"
              ) || t("text.noneText")}
            </div>
          </div>
          <div className={styles.infoDataApply}>
            <div className="text-left text-left--size-small mw-170">
              {t("text.link")}
            </div>
            <div className="text-2 text-2--size-small">
              {(
                <a
                  href={getValidHttpsUrl(
                    detailCandidate?.data?.applyInfo?.linkCvOnline
                  )}
                  target="_blank"
                >
                  {detailCandidate?.data?.applyInfo?.linkCvOnline}
                </a>
              ) || t("text.noneText")}
            </div>
          </div>
        </div>
      </div>
      {openCandidateInfoModal && (
        <CandidateInfoModal
          open={openCandidateInfoModal}
          toggle={() => setOpenCandidateInfoModal(!openCandidateInfoModal)}
          data={detailCandidate?.data}
        />
      )}
      {openApplyInfoModal && (
        <ApplyInfoModal
          open={openApplyInfoModal}
          toggle={() => setOpenApplyInfoModal(!openApplyInfoModal)}
          data={detailCandidate?.data}
        />
      )}
      {openBookingModal && (
        <ModalBookInterview
          open={openBookingModal}
          onCancel={() => setOpenBookingModal(false)}
          candidateId={id}
        />
      )}
      {openOfferModal && (
        <ModalOffer
          open={openOfferModal}
          onCancel={() => setOpenOfferModal(false)}
          candidateId={id}
        />
      )}
    </div>
  );
};

export default DetailCandidate;
