import {
  ListStatusCandidate,
  ListIdStatusCandidate,
} from "core/constants/status";
import {
  ROLE_SUPER_ADMIN_ID,
  ROLE_ADMIN_ID,
  ROLE_LEADER_ID,
  ROLE_HR_ID,
} from "core/constants/roleID";

export const arrLeaderPermission = [
  ROLE_SUPER_ADMIN_ID,
  ROLE_ADMIN_ID,
  ROLE_LEADER_ID,
];

export const arrHRPermission = [ROLE_SUPER_ADMIN_ID, ROLE_ADMIN_ID, ROLE_HR_ID];

export const getTextStatusCandidate = (status: number) => {
  return ListStatusCandidate.find((item) => item.status === status)?.name;
};

export const getColorStatusCandidate = (status: number) => {
  return ListStatusCandidate.find((item) => item.status === status)?.color;
};

export const getTextButtonDeny = (status: number, roleUser: number) => {
  switch (status) {
    case ListIdStatusCandidate.WAIT_LEADER_APPROVE_CV:
      return arrLeaderPermission.includes(roleUser) ? "Không duyệt CV" : null;
    case ListIdStatusCandidate.WAIT_LEADER_APPROVE_CANDIDATE:
      return arrLeaderPermission.includes(roleUser)
        ? "Không duyệt UV này"
        : null;
    case ListIdStatusCandidate.WAIT_CANDIDATE_RESPOND_OFFER:
      return arrHRPermission.includes(roleUser) ? "UV từ chối Offer" : null;
    default:
      return null;
  }
};

export const getTextButtonAccept = (status: number, roleUser: number) => {
  switch (status) {
    case ListIdStatusCandidate.WAIT_LEADER_APPROVE_CV:
      return arrLeaderPermission.includes(roleUser) ? "Duyệt CV" : null;
    case ListIdStatusCandidate.WAIT_INTERVIEW:
      return arrLeaderPermission.includes(roleUser) ? "Bắt đầu PV" : null;
    case ListIdStatusCandidate.WAIT_CANDIDATE_RESPOND_OFFER:
      return arrHRPermission.includes(roleUser) ? "UV đồng ý Offer" : null;
    default:
      return null;
  }
};

export const getNextStatusDeny = (status: number) => {
  switch (status) {
    case ListIdStatusCandidate.WAIT_LEADER_APPROVE_CV:
      return ListIdStatusCandidate.LEADER_NOT_APPROVE_CANDIDATE;
    case ListIdStatusCandidate.WAIT_LEADER_APPROVE_CANDIDATE:
      return ListIdStatusCandidate.LEADER_NOT_APPROVE_CANDIDATE;
    case ListIdStatusCandidate.WAIT_CANDIDATE_RESPOND_OFFER:
      return ListIdStatusCandidate.DENY_OFFER;
    default:
      return null;
  }
};

export const getNextStatusAccept = (status: number) => {
  switch (status) {
    case ListIdStatusCandidate.WAIT_LEADER_APPROVE_CV:
      return ListIdStatusCandidate.WAIT_CANDIDATE_RESPOND_INVITATION;
    case ListIdStatusCandidate.WAIT_INTERVIEW:
      return ListIdStatusCandidate.WAIT_LEADER_APPROVE_CANDIDATE;
    case ListIdStatusCandidate.WAIT_CANDIDATE_RESPOND_OFFER:
      return ListIdStatusCandidate.ONBOARD;
    default:
      return null;
  }
};
