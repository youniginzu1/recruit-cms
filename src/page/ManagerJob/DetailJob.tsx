import { useState } from "react";
import { useMutation, useQueryClient } from "react-query";
import { useTranslation } from "react-i18next";
import { useParams, useNavigate } from "react-router-dom";
import styled from "styled-components";
import { colors, images } from "core/assets";

import { Grid } from "@mui/material";
import { Button, Popconfirm, message } from "antd";

import { apiDeleteJob } from "core/api/job";
import { useGetDetailJob } from "core/hooks/job/useGetDetailJob";
import { getColorStatusJob } from "./helpers";

import BackHeader from "core/components/BackHeader";
import SubHeader from "core/components/SubHeader";
import GeneralInfo from "./components/GeneralInfo";
import OrderInfo from "./components/OrderInfo";
import CandidateInfo from "./components/CandidateInfo";
import ModalEditJob from "./components/ModalAddJob";
import { USE_GET_LIST_JOB } from "core/constants/queryName";
import { ListIdStatusCandidate } from "core/constants/status";

const DetailContainer = styled((props) => <Grid container {...props}></Grid>)`
  padding: 20px;
`;

const JobNameContainer = styled((props) => (
  <Grid {...props} container rowSpacing={0.8}></Grid>
))`
  padding-left: 20px !important;
  display: flex;
  align-items: center !important;
`;

const JobName = styled(Grid)`
  font-size: 16px;
  font-weight: bold;
  color: ${colors.mainColor};
  text-transform: uppercase;
`;

const CreatorJob = styled(Grid)`
  font-size: 12px;
  font-weight: 400;
  color: gray;
`;

const NumberCvContainer = styled((props) => (
  <Grid {...props} container columnSpacing={1}></Grid>
))`
  border-top: 1px solid #e7e7e7;
  border-bottom: 1px solid #e7e7e7;
`;

const BoxNumberCv = styled(({ numberCv, totalCv, ...props }) => (
  <Grid item xs={2}>
    <div {...props}>
      <img
        src={props.icon}
        alt={props.icon}
        className="mr-10"
        style={{ marginTop: "-30px" }}
      />
      <Grid container>
        <Grid
          item
          xs={12}
          sx={{
            fontSize: 13,
            color: `${getColorStatusJob(props.status, "#fea628")}`,
          }}
        >
          {props.title}
        </Grid>
        <Grid item xs={12} sx={{ fontSize: 22, fontWeight: 500 }}>
          {(numberCv && numberCv > 0 ? numberCv + "/" : "0/") + totalCv}
        </Grid>
      </Grid>
    </div>
  </Grid>
))`
  box-shadow: 0 5px 25px rgb(34 51 84 / 15%);
  padding: 10px;
  display: flex;
  border-radius: 5px;
  border: 1px solid ${(props) => getColorStatusJob(props.status, "#fea628")};
  background-color: ${(props) =>
    getColorStatusJob(props.status, "#fea628") + "25"};
`;

const DetailJob = () => {
  const { t } = useTranslation();
  const { id } = useParams();
  const navigate = useNavigate();
  const queryClient = useQueryClient();
  const { data: dataDetail } = useGetDetailJob(id);

  const [openModalEdit, setOpenModalEdit] = useState(false);
  const [openConfirmDelete, setOpenConfirmDelete] = useState(false);

  const dataJob = {
    id: dataDetail?.id,
    name: dataDetail?.name,
    position: dataDetail?.position,
    skill: dataDetail?.skill,
    level: dataDetail?.level,
    experience: dataDetail?.experience,
    max_salary: dataDetail?.max_salary,
    description: dataDetail?.description,
  };

  const dataGeneral = {
    position: dataDetail?.position.name as string,
    level: dataDetail?.level.name as string,
    skill: dataDetail?.skill.name as string,
    experience: dataDetail?.experience.name as string,
    maxSalary: dataDetail?.max_salary,
    status: dataDetail?.status ?? 1,
    description: dataDetail?.description as string,
  };

  const totalCV = dataDetail?.applyInfo?.length;
  const acceptPV = dataDetail?.applyInfo?.filter(
    (item: any) => item.status >= ListIdStatusCandidate.WAIT_INTERVIEW
  )?.length;
  const interviewed = dataDetail?.applyInfo?.filter(
    (item: any) =>
      item.status >= ListIdStatusCandidate.WAIT_LEADER_APPROVE_CANDIDATE
  )?.length;
  const passedPV = dataDetail?.applyInfo?.filter(
    (item: any) =>
      item.status >= ListIdStatusCandidate.WAIT_CANDIDATE_RESPOND_OFFER
  );
  const acceptOffer = dataDetail?.applyInfo?.filter(
    (item: any) => item.status === ListIdStatusCandidate.ONBOARD
  );
  const denyOffer = dataDetail?.applyInfo?.filter(
    (item: any) => item.status === ListIdStatusCandidate.DENY_OFFER
  );

  const { mutate: deleteJob, isLoading: isLoadingDeleteJob } = useMutation(
    (id: number) => apiDeleteJob(id),
    {
      onSuccess: () => {
        queryClient.invalidateQueries(USE_GET_LIST_JOB);
        message.success(t("message.deleteJobSuccess"));
        navigate(-1);
      },
    }
  );

  return (
    <DetailContainer>
      <Grid item xs={12} container className="d-flex-sb-c">
        <BackHeader title={t("text.detail")} />
        <div className="d-flex-sb-c">
          <Popconfirm
            title={t("alert.areYouSureDeleteJob")}
            open={openConfirmDelete}
            onConfirm={() => deleteJob(dataDetail?.id)}
            onCancel={() => setOpenConfirmDelete(false)}
            okButtonProps={{ loading: isLoadingDeleteJob }}
          >
            <Button
              onClick={() => setOpenConfirmDelete(true)}
              className="btn mw-100 mr-10"
            >
              {t("btn.deleteJob")}
            </Button>
          </Popconfirm>
          <Button onClick={() => setOpenModalEdit(true)} className="btn mw-100">
            {t("btn.editInfo")}
          </Button>
        </div>
      </Grid>
      <Grid item xs={12} mt={1.5} container className="d-flex-sb-c">
        <JobNameContainer item xs={3.5}>
          <JobName item xs={12}>
            {dataDetail?.name}
          </JobName>
          <CreatorJob item xs={12}>
            {t("managerJob.creatorJob") + ": " + dataDetail?.assigned_info.name}
          </CreatorJob>
        </JobNameContainer>
        <NumberCvContainer item xs={8.5}>
          <BoxNumberCv
            status={1} //blue
            title={t("managerJob.numberCv")}
            numberCv={totalCV}
            totalCv={totalCV}
            icon={images.jobCvIconBlue}
          />
          <BoxNumberCv
            title={t("managerJob.acceptInterview")}
            numberCv={acceptPV}
            totalCv={totalCV}
            icon={images.jobCvIconYellow}
          />
          <BoxNumberCv
            status={3} //green
            title={t("managerJob.interviewed")}
            numberCv={interviewed}
            totalCv={totalCV}
            icon={images.jobCvIconGreen}
          />
          <BoxNumberCv
            status={3} //green
            title={t("managerJob.passedInterview")}
            numberCv={passedPV}
            totalCv={totalCV}
            icon={images.jobCvIconGreen}
          />
          <BoxNumberCv
            status={3} //green
            title={t("managerJob.acceptOffer")}
            numberCv={acceptOffer}
            totalCv={totalCV}
            icon={images.jobCvIconGreen}
          />
          <BoxNumberCv
            status={2} //red
            title={t("managerJob.refuseOffer")}
            numberCv={denyOffer}
            totalCv={totalCV}
            icon={images.jobCvIconRed}
          />
        </NumberCvContainer>
      </Grid>
      <Grid item xs={12} container>
        <SubHeader
          listTab={[
            {
              label: t("tab.generalInfo"),
              key: "generalInfo",
              element: <GeneralInfo data={dataGeneral} />,
            },
            {
              label: t("tab.orderInfo"),
              key: "orderInfo",
              element: (
                <OrderInfo jobId={dataDetail?.id} data={dataDetail?.orders} />
              ),
            },
            {
              label: t("tab.candidateInfo"),
              key: "candidateInfo",
              element: (
                <CandidateInfo
                  jobId={dataDetail?.id}
                  data={dataDetail?.applyInfo}
                />
              ),
            },
          ]}
        />
      </Grid>
      {openModalEdit && (
        <ModalEditJob
          open={openModalEdit}
          toggle={() => setOpenModalEdit(false)}
          dataJob={dataJob}
        />
      )}
    </DetailContainer>
  );
};

export default DetailJob;
