import { useState } from "react";
import { useForm } from "react-hook-form";
import { useTranslation } from "react-i18next";
import { useMutation, useQueryClient } from "react-query";
import styled from "styled-components";

import { Modal, Input, List, message, Spin } from "antd";
import { Grid } from "@mui/material";

import { images, colors } from "core/assets";
import FooterModalSubmit from "core/components/FooterModalSubmit";
import { apiAddCandidateToJob } from "core/api/job";
import { useGetListCandidateAvailable } from "core/hooks/job/useGetListCandidateAvailable";
import { IFilter } from "core/constants/interfaces";
import useFilter from "core/hooks/common/useFilter";
import { useAuth } from "auth/contexts/AuthProvider";
import Autocomplete from "core/components/ReactHookForm/Autocomplete";
import { convertUTCToLocalString } from "core/helpers/index";
import useTransformIdToText from "core/hooks/common/useTransformIdToText";
import { USE_GET_DETAIL_JOB } from "core/constants/queryName";

const TitleInfo = styled.span`
  font-weight: 700;
  font-size: 16px;
  line-height: 19px;
  color: ${colors.mainColor};
`;

const ListItemCustom = styled(({ isSelected, ...props }) => (
  <List.Item {...props}></List.Item>
))`
  background-color: ${(props) => props.isSelected && colors.mainColorRGB};
  margin-right: -24px;
  padding-right: 24px;
  margin-left: -24px;
  padding-left: 24px;
  cursor: pointer;
`;

interface IProps {
  open: boolean;
  toggle: () => void;
  jobId: number;
}

const defaultFilter: IFilter = {};

const ModalAddCandidateToJob = (props: IProps) => {
  const { open, toggle, jobId } = props;
  const { t } = useTranslation();
  const queryClient = useQueryClient();
  const { listPosition, listSkill, listLevel } = useAuth();
  const { transformGender, transformPosition, transformSkill, transformLevel } =
    useTransformIdToText();
  const { filter, handleFilterChange, handleSearch } = useFilter(defaultFilter);

  const [candidateIds, setCandidateIds] = useState<number[]>([]);

  const { data: listCandidate, isLoading: isLoadingGetListCandidate } =
    useGetListCandidateAvailable(filter);

  const { control, setValue } = useForm({});

  const { mutate: addCandidateToJob, isLoading: isAddingCandidate } =
    useMutation(() => apiAddCandidateToJob({ candidateIds, jobId }), {
      onSuccess: () => {
        queryClient.invalidateQueries(USE_GET_DETAIL_JOB);
        message.success(t("message.addCandidateToJobSuccess"));
        toggle();
      },
    });

  const handleSelected = (id: number) => {
    if (candidateIds.includes(id))
      setCandidateIds(candidateIds.filter((item) => item !== id));
    else setCandidateIds([...candidateIds, id]);
  };

  return (
    <Modal
      title={t("modal.addCandidateToJob")}
      open={open}
      className="modal-custom"
      centered
      onCancel={toggle}
      footer={
        <FooterModalSubmit
          onOk={() => addCandidateToJob()}
          isLoadingOnOk={isAddingCandidate}
        />
      }
    >
      <Grid container rowSpacing={1.5}>
        <Grid item xs={12}>
          <Input
            placeholder={t("placeholder.keyword")}
            prefix={<img src={images.searchIcon} />}
            className="inputCustom fs-12"
            onChange={(text) => {
              handleSearch.keywordSearch(text);
            }}
          />
        </Grid>
        <Grid item xs={12} container columnSpacing={1.5}>
          <Grid item xs={4}>
            <Autocomplete
              name="positionId"
              label={t("common.position")}
              control={control}
              listOptions={listPosition}
              getOptionLabel={(option) => option.name}
              customRenderOption={(option) => <span>{option.name}</span>}
              onChange={(option) => {
                setValue("positionId", option?.id ?? null);
                handleFilterChange({ positionId: option?.id });
              }}
            />
          </Grid>
          <Grid item xs={4}>
            <Autocomplete
              name="skillId"
              label={t("common.skill")}
              control={control}
              listOptions={listSkill}
              getOptionLabel={(option) => option.name}
              customRenderOption={(option) => <span>{option.name}</span>}
              onChange={(option) => {
                setValue("skillId", option?.id ?? null);
                handleFilterChange({ skillId: option?.id });
              }}
            />
          </Grid>
          <Grid item xs={4}>
            <Autocomplete
              name="levelId"
              label={t("common.level")}
              control={control}
              listOptions={listLevel}
              getOptionLabel={(option) => option.name}
              customRenderOption={(option) => <span>{option.name}</span>}
              onChange={(option) => {
                setValue("levelId", option?.id ?? null);
                handleFilterChange({ levelId: option?.id });
              }}
            />
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <TitleInfo>{t("form.listCandidate")}</TitleInfo>
        </Grid>
        <Grid item xs={12} id="listCandidate">
          {isLoadingGetListCandidate && <Spin />}
          {!isLoadingGetListCandidate && (
            <List
              dataSource={listCandidate}
              renderItem={(item: any) => (
                <ListItemCustom
                  key={item.id}
                  onClick={() => handleSelected(item.id)}
                  isSelected={candidateIds.includes(item.id)}
                >
                  <List.Item.Meta
                    title={
                      <a
                        href={`/detail-candidate/${item.id}`}
                        target="_blank"
                      >
                        {item.fullName}
                      </a>
                    }
                    description={
                      transformPosition(item.applyInfo.positionId) +
                      " - " +
                      transformSkill(item.applyInfo.skillId) +
                      " - " +
                      transformLevel(item.applyInfo.levelId) +
                      "/ " +
                      transformGender(item.sex) +
                      " - " +
                      convertUTCToLocalString(item.birthday, "DD/MM/YYYY")
                    }
                  />
                </ListItemCustom>
              )}
            />
          )}
        </Grid>
      </Grid>
    </Modal>
  );
};

export default ModalAddCandidateToJob;
