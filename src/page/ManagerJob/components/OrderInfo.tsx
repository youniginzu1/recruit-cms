import { useState } from "react";
import { useTranslation } from "react-i18next";
import styled from "styled-components";

import { Table } from "antd";
import AddIcon from "@mui/icons-material/Add";
import type { ColumnsType } from "antd/es/table";

import { colors } from "core/assets";
import { IOrderInfo } from "core/constants/interfaces";

import ModalAddOrderToJob from "./ModalAddOrderToJob";
import { convertUTCToLocalString } from "core/helpers";
import {
  getColorStatusOrder,
  getTextStatusOrder,
} from "page/ManagerOrder/helpers";

const Container = styled.div`
  border: 1px solid #e7e7e7;
  padding: 20px;
`;

const ButtonAdd = styled.button`
  background: transparent;
  border-radius: 50%;
  color: #fea628;
  border: 1px dashed #fea628;
  width: 35px;
  height: 35px;
  display: flex;
  align-items: center;
  justify-content: center;
  &:hover {
    transform: scale(1.2);
    cursor: pointer;
  }
`;

const TitleInfo = styled.span`
  font-weight: 700;
  font-size: 16px;
  line-height: 19px;
  color: ${colors.mainColor};
`;

interface IProps {
  jobId: number;
  data: any;
}

const OrderInfo = (props: IProps) => {
  const { jobId, data } = props;
  const { t } = useTranslation();
  const [isOpenModalAddCandidate, setOpenModalAddCandidate] = useState(false);

  const columns: ColumnsType<IOrderInfo> = [
    {
      title: t("table.index"),
      dataIndex: "index",
      key: "index",
      render: (value, record, index) => <span>{index + 1}</span>,
      width: "5%",
      align: "center",
    },
    {
      title: t("managerOrder.orderName"),
      dataIndex: "name",
      width: "22%",
    },
    {
      title: t("common.creator"),
      dataIndex: "creator",
      render: (value, record, index) => <span>{record?.owner?.name}</span>,
      width: "22%",
    },
    {
      title: t("common.status"),
      dataIndex: "status",
      render: (value, record, index) => (
        <span style={{ color: getColorStatusOrder(record?.status) }}>
          {getTextStatusOrder(record?.status)}
        </span>
      ),
      width: "14%",
    },
    {
      title: t("common.deadline"),
      dataIndex: "deadline",
      render: (value, record, index) => (
        <span>{convertUTCToLocalString(record?.deadline, "DD/MM/YYYY")}</span>
      ),
      width: "5%",
    },
    {
      title: t("managerJob.totalRecruited"),
      dataIndex: "totalRecruited",
      render: (value, record, index) => (
        <span>{record?.totalRecruited ?? 0 + "/" + record?.target}</span>
      ),
      align: "center",
    },
    {
      title: t("managerOrder.linkOrder"),
      dataIndex: "link",
      render: (value, record, index) => (
        <a href={`/manager-order/detail-order/${record?.id}`}>
          {t("common.seeDetail")}
        </a>
      ),
      width: "10%",
      align: "center",
    },
  ];

  return (
    <Container>
      <div className="d-flex-sb-c">
        <TitleInfo>{t("text.totalOrder") + ": " + data?.length}</TitleInfo>
        <ButtonAdd onClick={() => setOpenModalAddCandidate(true)}>
          <AddIcon />
        </ButtonAdd>
      </div>
      <Table
        columns={columns}
        dataSource={data}
        pagination={false}
        bordered
        rowKey={(obj) => obj.id}
        className="tableCus table--rowSelect mt-10"
      />
      {isOpenModalAddCandidate && (
        <ModalAddOrderToJob
          open={isOpenModalAddCandidate}
          toggle={() => setOpenModalAddCandidate(false)}
          jobId={jobId}
        />
      )}
    </Container>
  );
};

export default OrderInfo;
