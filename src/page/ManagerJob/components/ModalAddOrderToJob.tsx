import { useState } from "react";
import { useForm } from "react-hook-form";
import { useTranslation } from "react-i18next";
import { useMutation, useQueryClient } from "react-query";
import styled from "styled-components";

import { Modal, Input, List, message, Spin } from "antd";
import { Grid } from "@mui/material";

import { images, colors } from "core/assets";
import FooterModalSubmit from "core/components/FooterModalSubmit";
import { apiAddOrderToJob } from "core/api/job";
import { useGetListOrderAvailable } from "core/hooks/job/useGetListOrderAvailable";
import { IFilter } from "core/constants/interfaces";
import useFilter from "core/hooks/common/useFilter";
import { useAuth } from "auth/contexts/AuthProvider";
import Autocomplete from "core/components/ReactHookForm/Autocomplete";
import { convertUTCToLocalString } from "core/helpers/index";
import { USE_GET_DETAIL_JOB } from "core/constants/queryName";

const TitleInfo = styled.span`
  font-weight: 700;
  font-size: 16px;
  line-height: 19px;
  color: ${colors.mainColor};
`;

const ListItemCustom = styled(({ isSelected, ...props }) => (
  <List.Item {...props}></List.Item>
))`
  background-color: ${(props) => props.isSelected && colors.mainColorRGB};
  margin-right: -24px;
  padding-right: 24px;
  margin-left: -24px;
  padding-left: 24px;
  cursor: pointer;
`;

interface IProps {
  open: boolean;
  toggle: () => void;
  jobId: number;
}

const defaultFilter: IFilter = {};

const ModalAddOrderToJob = (props: IProps) => {
  const { open, toggle, jobId } = props;
  const { t } = useTranslation();
  const queryClient = useQueryClient();
  const { listPosition, listSkill, listLevel } = useAuth();
  const { filter, handleFilterChange, handleSearch } = useFilter(defaultFilter);

  const [orderIds, setOrderIds] = useState<number[]>([]);

  const { data: listOrder, isLoading: isLoadingGetListOrder } =
    useGetListOrderAvailable(filter);

  const { control, setValue } = useForm({});

  const { mutate: addOrderToJob, isLoading: isAddingOrder } = useMutation(
    () => apiAddOrderToJob({ orderIds, jobId }),
    {
      onSuccess: () => {
        queryClient.invalidateQueries(USE_GET_DETAIL_JOB);
        message.success(t("message.addOrderToJobSuccess"));
        toggle();
      },
    }
  );

  const handleSelected = (id: number) => {
    if (orderIds.includes(id))
      setOrderIds(orderIds.filter((item) => item !== id));
    else setOrderIds([...orderIds, id]);
  };

  return (
    <Modal
      title={t("modal.addOrderToJob")}
      open={open}
      className="modal-custom"
      centered
      onCancel={toggle}
      footer={
        <FooterModalSubmit
          onOk={() => addOrderToJob()}
          isLoadingOnOk={isAddingOrder}
        />
      }
    >
      <Grid container rowSpacing={1.5}>
        <Grid item xs={12}>
          <Input
            placeholder={t("placeholder.keyword")}
            prefix={<img src={images.searchIcon} />}
            className="inputCustom fs-12"
            onChange={(text) => {
              handleSearch.keywordSearch(text);
            }}
          />
        </Grid>
        <Grid item xs={12} container columnSpacing={1.5}>
          <Grid item xs={4}>
            <Autocomplete
              name="positionId"
              label={t("common.position")}
              control={control}
              listOptions={listPosition}
              getOptionLabel={(option) => option.name}
              customRenderOption={(option) => <span>{option.name}</span>}
              onChange={(option) => {
                setValue("positionId", option?.id ?? null);
                handleFilterChange({ positionId: option?.id });
              }}
            />
          </Grid>
          <Grid item xs={4}>
            <Autocomplete
              name="skillId"
              label={t("common.skill")}
              control={control}
              listOptions={listSkill}
              getOptionLabel={(option) => option.name}
              customRenderOption={(option) => <span>{option.name}</span>}
              onChange={(option) => {
                setValue("skillId", option?.id ?? null);
                handleFilterChange({ skillId: option?.id });
              }}
            />
          </Grid>
          <Grid item xs={4}>
            <Autocomplete
              name="levelId"
              label={t("common.level")}
              control={control}
              listOptions={listLevel}
              getOptionLabel={(option) => option.name}
              customRenderOption={(option) => <span>{option.name}</span>}
              onChange={(option) => {
                setValue("levelId", option?.id ?? null);
                handleFilterChange({ levelId: option?.id });
              }}
            />
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <TitleInfo>{t("form.listOrder")}</TitleInfo>
        </Grid>
        <Grid item xs={12} id="listOrder">
          {isLoadingGetListOrder && <Spin />}
          {!isLoadingGetListOrder && (
            <List
              dataSource={listOrder}
              renderItem={(item: any) => (
                <ListItemCustom
                  key={item.id}
                  onClick={() => handleSelected(item.id)}
                  isSelected={orderIds.includes(item.id)}
                >
                  <List.Item.Meta
                    title={
                      <a
                        href={`/manager-order/detail-order/${item.id}`}
                        target="_blank"
                      >
                        {item?.name}
                      </a>
                    }
                    description={
                      item?.position?.name +
                      " - " +
                      item?.skill?.name +
                      " - " +
                      item?.level?.name +
                      "/ Deadline: " +
                      convertUTCToLocalString(item?.deadline, "DD/MM/YYYY")
                    }
                  />
                </ListItemCustom>
              )}
            />
          )}
        </Grid>
      </Grid>
    </Modal>
  );
};

export default ModalAddOrderToJob;
