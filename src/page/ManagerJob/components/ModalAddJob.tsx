import { useTranslation } from "react-i18next";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useMutation, useQueryClient } from "react-query";
import styled from "styled-components";
import { colors } from "core/assets";

import { Grid } from "@mui/material";
import { Modal, message } from "antd";

import { apiCreateJob, apiEditJob } from "core/api/job";
import { IJobForm } from "core/constants/interfaces";
import FooterModalSubmit from "core/components/FooterModalSubmit";
import TextField from "core/components/ReactHookForm/TextField";
import Autocomplete from "core/components/ReactHookForm/Autocomplete";
import { useAuth } from "auth/contexts/AuthProvider";
import { USE_GET_LIST_JOB, USE_GET_DETAIL_JOB } from "core/constants/queryName";

interface IProps {
  open: boolean;
  toggle: () => void;
  dataJob?: any;
}

const TitleInfo = styled.span`
  font-weight: 700;
  font-size: 16px;
  line-height: 19px;
  color: ${colors.mainColor};
`;

const ModalAddJob = (props: IProps) => {
  const { open, toggle, dataJob } = props;
  const { t } = useTranslation();
  const queryClient = useQueryClient();
  const { listPosition, listSkill, listLevel, listExperience } = useAuth();

  const {
    handleSubmit,
    control,
    setValue,
    clearErrors,
    formState: { errors },
  } = useForm<IJobForm>({
    resolver: yupResolver(
      yup.object({
        name: yup
          .string()
          .nullable()
          .required(t("validate.required") as string)
          .min(6, t("validate.nameMin6") as string),
        positionId: yup
          .string()
          .required(t("validate.required") as string)
          .nullable(),
        skillId: yup
          .string()
          .required(t("validate.required") as string)
          .nullable(),
        levelId: yup
          .string()
          .required(t("validate.required") as string)
          .nullable(),
        experience: yup
          .string()
          .required(t("validate.required") as string)
          .nullable(),
        max_salary: yup
          .number()
          .required(t("validate.required") as string)
          .typeError(t("validate.enterCorrectFormat") as string),
      })
    ),
    defaultValues: {
      name: dataJob?.name,
      positionId: dataJob?.position.id,
      skillId: dataJob?.skill.id,
      levelId: dataJob?.level.id,
      experience: dataJob?.experience.id,
      max_salary: dataJob?.max_salary,
      description: dataJob?.description,
    },
    mode: "onChange",
  });

  const { mutate: createJob, isLoading: isLoadingCreateJob } = useMutation(
    (params: any) => {
      if (!!dataJob) return apiEditJob(dataJob.id, params);
      return apiCreateJob(params);
    },
    {
      onSuccess: () => {
        if (!!dataJob) queryClient.invalidateQueries(USE_GET_DETAIL_JOB);
        queryClient.invalidateQueries(USE_GET_LIST_JOB);
        message.success(
          !!dataJob ? t("message.editJobSuccess") : t("message.addJobSuccess")
        );
        toggle();
      },
    }
  );

  return (
    <Modal
      title={dataJob ? t('modal.editJob') : t("modal.addJobTitle")}
      open={open}
      className="modal-custom"
      centered
      onCancel={toggle}
      footer={
        <FooterModalSubmit
          onOk={handleSubmit((values) => createJob(values))}
          onCancel={toggle}
          isLoadingOnOk={isLoadingCreateJob}
        />
      }
    >
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <TitleInfo>{t("form.generalInfoTitle")}</TitleInfo>
        </Grid>
        <Grid item xs={12}>
          <TextField
            name="name"
            label={t("managerJob.jobName")}
            errors={errors}
            control={control}
            required
          />
        </Grid>
        <Grid item xs={6}>
          <Autocomplete
            name="positionId"
            label={t("common.position")}
            control={control}
            errors={errors}
            clearErrors={clearErrors}
            listOptions={listPosition}
            defaultValue={dataJob?.position}
            getOptionLabel={(option) => option.name}
            customRenderOption={(option) => <span>{option.name}</span>}
            onChange={(option) => setValue("positionId", option?.id ?? null)}
            required
          />
        </Grid>
        <Grid item xs={6}>
          <Autocomplete
            name="skillId"
            label={t("common.skill")}
            control={control}
            errors={errors}
            clearErrors={clearErrors}
            listOptions={listSkill}
            defaultValue={dataJob?.skill}
            getOptionLabel={(option) => option.name}
            customRenderOption={(option) => <span>{option.name}</span>}
            onChange={(option) => setValue("skillId", option?.id ?? null)}
            required
          />
        </Grid>
        <Grid item xs={6}>
          <Autocomplete
            name="levelId"
            label={t("common.level")}
            control={control}
            errors={errors}
            clearErrors={clearErrors}
            listOptions={listLevel}
            defaultValue={dataJob?.level}
            getOptionLabel={(option) => option.name}
            customRenderOption={(option) => <span>{option.name}</span>}
            onChange={(option) => setValue("levelId", option?.id ?? null)}
            required
          />
        </Grid>
        <Grid item xs={6}>
          <Autocomplete
            name="experience"
            label={t("common.experience")}
            control={control}
            errors={errors}
            clearErrors={clearErrors}
            listOptions={listExperience}
            defaultValue={dataJob?.experience}
            getOptionLabel={(option) => option.name}
            customRenderOption={(option) => <span>{option.name}</span>}
            onChange={(option) => setValue("experience", option?.id ?? null)}
            required
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            name="max_salary"
            label={t("managerJob.salaryUpTo")}
            errors={errors}
            control={control}
            type="number"
            required
          />
        </Grid>
        <Grid item xs={12}>
          <TitleInfo>{t("form.description")}</TitleInfo>
        </Grid>
        <Grid item xs={12}>
          <TextField
            name="description"
            errors={errors}
            control={control}
            rows={3}
          />
        </Grid>
      </Grid>
    </Modal>
  );
};

export default ModalAddJob;
