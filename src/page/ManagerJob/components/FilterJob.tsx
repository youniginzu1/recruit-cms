// import { useState } from "react";
import { useTranslation } from "react-i18next";
import styled from "styled-components";

import { Button, Input } from "antd";
import { Grid } from "@mui/material";

import { images } from "core/assets";
import InputFilterByModal from "core/components/InputFilterByModal";
import { useAuth } from "auth/contexts/AuthProvider";
import { StatusJob } from "core/constants/enums";
import { getColorStatusJob } from "../helpers";

const FilterContainer = styled((props) => <Grid container {...props}></Grid>)`
  max-width: 304px;
  background: #fff;
  border: 1px solid #d6d6d6;
  box-shadow: 0px 5px 25px rgb(34 51 84 / 15%);
  border-radius: 5px;
  padding: 19px;
  padding-bottom: 5px;
`;

const CountContainer = styled((props) => (
  <Grid item xs={12} mb={1.5} {...props}></Grid>
))`
  display: flex;
  justify-content: space-between;
  align-items: center;
  border-radius: 5px;
  padding: 6px 12px;
  background-color: ${(props) =>
    getColorStatusJob(props.status, "#ffdba9") + "30"};
  border: 1px solid ${(props) => getColorStatusJob(props.status, "#ffdba9")};
  font-size: 14px;
  font-weight: 500;
  color: ${(props) => getColorStatusJob(props.status)};
`;

interface IProps {
  filter?: any;
  handleSearch?: any;
  resetFilter?: any;
  handleFilterChange?: any;
  totalItems?: number;
  metaData?: any;
}

const FilterJob = (props: IProps) => {
  const { t } = useTranslation();
  const { handleSearch, filter, resetFilter, handleFilterChange, totalItems, metaData } = props;
  const { listPosition, listSkill, listLevel } = useAuth();

  const listStatus = [
    { id: StatusJob.PROCESSING, name: t("managerJob.status.processing") },
    { id: StatusJob.LATE_DEADLINE, name: t("managerJob.status.lateDeadline") },
    { id: StatusJob.COMPLETED, name: t("managerJob.status.completed") },
  ];

  return (
    <FilterContainer>
      <CountContainer>
        <span>{t("managerJob.totalJob")}</span>
        <span>
          {metaData?.totalJobPending +
            metaData?.totalJobComplete +
            metaData?.totalJobLateDeadline}
        </span>
      </CountContainer>
      <CountContainer status={StatusJob.PROCESSING}>
        <span>{t("managerJob.status.processing")}</span>
        <span>{metaData?.totalJobPending}</span>
      </CountContainer>
      <CountContainer status={StatusJob.COMPLETED}>
        <span>{t("managerJob.status.completed")}</span>
        <span>{metaData?.totalJobComplete}</span>
      </CountContainer>
      <CountContainer status={StatusJob.LATE_DEADLINE}>
        <span>{t("managerJob.status.lateDeadline")}</span>
        <span>{metaData?.totalJobLateDeadline}</span>
      </CountContainer>
      <Grid item xs={12} pb={1.5}>
        <Input
          placeholder={t("placeholder.keyword")}
          prefix={<img src={images.searchIcon} />}
          className="inputCustom fs-12"
          onChange={(text) => handleSearch.keywordSearch(text)}
        />
      </Grid>
      <Grid item xs={12} pb={1.5}>
        <InputFilterByModal
          titleModal={t("modal.filterByPosition")}
          placeholderInput={t("common.position")}
          listFilter={listPosition}
          itemIdSelected={filter?.positionId}
          setItemIdSelected={(id: number) =>
            handleFilterChange({ positionId: id })
          }
        />
      </Grid>
      <Grid item xs={12} pb={1.5}>
        <InputFilterByModal
          titleModal={t("modal.filterBySkill")}
          placeholderInput={t("common.skill")}
          listFilter={listSkill}
          itemIdSelected={filter?.skillId}
          setItemIdSelected={(id: number) =>
            handleFilterChange({ skillId: id })
          }
        />
      </Grid>
      <Grid item xs={12} pb={1.5}>
        <InputFilterByModal
          titleModal={t("modal.filterByLevel")}
          placeholderInput={t("common.level")}
          listFilter={listLevel}
          itemIdSelected={filter?.levelId}
          setItemIdSelected={(id: number) =>
            handleFilterChange({ levelId: id })
          }
        />
      </Grid>
      <Grid item xs={12} pb={1.5}>
        <InputFilterByModal
          titleModal={t("modal.filterByStatus")}
          placeholderInput={t("common.status")}
          listFilter={listStatus}
          itemIdSelected={filter?.status}
          setItemIdSelected={(id: number) => handleFilterChange({ status: id })}
        />
      </Grid>
      <Grid item xs={12} pb={1.5}>
        <Button
          className="btn btnForm btn--reset"
          icon={<img src={images.refreshIcon} />}
          onClick={() => resetFilter()}
        >
          {t("btn.reset")}
        </Button>
      </Grid>
    </FilterContainer>
  );
};

export default FilterJob;
