import { useState } from "react";
import { useTranslation } from "react-i18next";
import { useMutation, useQueryClient } from "react-query";
import styled from "styled-components";

import { Table, Popconfirm, message } from "antd";
import AddIcon from "@mui/icons-material/Add";
import DeleteIcon from "@mui/icons-material/Delete";
import type { ColumnsType } from "antd/es/table";

import { colors } from "core/assets";
import { ICandidateInfo } from "core/constants/interfaces";
import { apiRemoveCandidate } from "core/api/job";

import ModalAddCandidateToJob from "./ModalAddCandidateToJob";
import { USE_GET_DETAIL_JOB } from "core/constants/queryName";
import { getColorStatusCandidate, getTextStatusCandidate } from "page/ManagerCandidate/helpers";

const Container = styled.div`
  border: 1px solid #e7e7e7;
  padding: 20px;
`;

const ButtonAdd = styled.button`
  background: transparent;
  border-radius: 50%;
  color: #fea628;
  border: 1px dashed #fea628;
  width: 35px;
  height: 35px;
  display: flex;
  align-items: center;
  justify-content: center;
  &:hover {
    transform: scale(1.2);
    cursor: pointer;
  }
`;

const ButtonDelete = styled.button`
  background: transparent;
  border-radius: 50%;
  border: 1px dashed gray;
  width: 30px;
  height: 30px;
  display: flex;
  align-items: center;
  justify-content: center;
  &:hover {
    transform: scale(1.2);
    cursor: pointer;
  }
`;

const TitleInfo = styled.span`
  font-weight: 700;
  font-size: 16px;
  line-height: 19px;
  color: ${colors.mainColor};
`;

interface IProps {
  jobId: number;
  data: any;
}

const CandidateInfo = (props: IProps) => {
  const { jobId, data } = props;
  const { t } = useTranslation();
  const queryClient = useQueryClient();
  const [isOpenModalAddCandidate, setOpenModalAddCandidate] = useState(false);
  const [openConfirmDelete, setOpenConfirmDelete] = useState<any>({});

  const { mutate: deleteCandidate, isLoading: isLoadingDelete } = useMutation(
    (params: any) => apiRemoveCandidate(params),
    {
      onSuccess: () => {
        queryClient.invalidateQueries(USE_GET_DETAIL_JOB);
        message.success(t("message.deleteCandidateOutJobSuccess"));
      },
    }
  );

  const columns: ColumnsType<ICandidateInfo> = [
    {
      title: t("table.index"),
      dataIndex: "index",
      key: "index",
      render: (value, record, index) => <span>{index + 1}</span>,
      width: "5%",
      align: "center",
    },
    {
      title: t("managerCandidate.candidateName"),
      dataIndex: "fullName",
      render: (value, record, index) => (
        <span>{record?.candidate?.fullName}</span>
      ),
      width: "26%",
    },
    {
      title: t("common.status"),
      dataIndex: "status",
      render: (value, record, index) => (
        <span
          style={{ color: getColorStatusCandidate(record?.status) }}
        >
          {t(
            getTextStatusCandidate(record?.status) || "text.noneText"
          )}
        </span>
      ),
      width: "26%",
    },
    {
      title: t("managerCandidate.HRAssigned"),
      dataIndex: "hrAssigned",
      render: (value, record, index) => (
        <span>{record?.candidate?.hrAssigned?.name}</span>
      ),
      width: "26%",
    },
    {
      title: t("managerCandidate.linkCandidate"),
      dataIndex: "link",
      render: (value, record, index) => (
        <a href={`/detail-candidate/${record?.id}`}>
          {t("common.seeDetail")}
        </a>
      ),
      width: "12%",
      align: "center",
    },
    {
      title: "",
      dataIndex: "iconRemove",
      align: "center",
      render: (value, record, index) => (
        <Popconfirm
          placement="topRight"
          title={t("alert.areYouSureDeleteCandidateOutJob")}
          open={openConfirmDelete?.[record.id]}
          onConfirm={() =>
            deleteCandidate({ jobId, candidateIds: [record.id] })
          }
          onCancel={() =>
            setOpenConfirmDelete({
              ...openConfirmDelete,
              [`${record?.id}`]: false,
            })
          }
          okButtonProps={{ loading: isLoadingDelete }}
        >
          <ButtonDelete
            onClick={() =>
              setOpenConfirmDelete({
                ...openConfirmDelete,
                [`${record?.id}`]: true,
              })
            }
          >
            <DeleteIcon />
          </ButtonDelete>
        </Popconfirm>
      ),
    },
  ];

  return (
    <Container>
      <div className="d-flex-sb-c">
        <TitleInfo>{t("text.totalCandidate") + ": " + data?.length}</TitleInfo>
        <ButtonAdd onClick={() => setOpenModalAddCandidate(true)}>
          <AddIcon />
        </ButtonAdd>
      </div>
      <Table
        columns={columns}
        dataSource={data}
        pagination={false}
        bordered
        rowKey={(obj) => obj.id}
        className="tableCus table--rowSelect mt-10"
      />
      {isOpenModalAddCandidate && (
        <ModalAddCandidateToJob
          open={isOpenModalAddCandidate}
          toggle={() => setOpenModalAddCandidate(false)}
          jobId={jobId}
        />
      )}
    </Container>
  );
};

export default CandidateInfo;
