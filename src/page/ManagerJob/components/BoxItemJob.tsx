import styled from "styled-components";
import { useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { StatusJob } from "core/constants/enums";
import { colors, images } from "core/assets";
import { getColorStatusJob, getTextStatus } from "../helpers";

const BoxContainer = styled((props) => <div {...props}></div>)`
  width: 100%;
  border: 1px solid gray;
  border-top: ${(props) => `3px solid ${getColorStatusJob(props.status)}`};
  border-radius: 5px;
  padding: 15px;
  cursor: pointer;
  background: #fff7ec;
`;

const JobNameRow = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
`;

const JobName = styled.span`
  color: ${colors.mainColor};
  font-weight: 700;
  font-size: 16px;
`;

const RowContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const TitleRow = styled.span`
  font-size: 13px;
  color: #5a6882;
`;

const ContentRow = styled((props) => <span {...props}></span>)`
  color: ${(props) => getColorStatusJob(props?.status)};
  font-weight: 700;
  font-size: 13px;
`;

interface IProps {
  info: {
    id: number;
    name: string;
    position: string;
    level: string;
    skill: string;
    status: number;
    totalTarget?: number;
    totalCandidateOnboard?: number;
    totalOrder?: number;
  };
}

const BoxItemJob = (props: IProps) => {
  const {
    id,
    name,
    position = "N/A",
    level = "N/A",
    skill = "N/A",
    totalTarget = 0,
    totalCandidateOnboard = 0,
    totalOrder = 0,
    status,
  } = props.info;
  const { t } = useTranslation();
  const navigate = useNavigate();

  const getImageAcc = (status: number) => {
    switch (status) {
      case StatusJob.PROCESSING:
        return images.jobIconBlue;
      case StatusJob.LATE_DEADLINE:
        return images.jobIconRed;
      case StatusJob.COMPLETED:
        return images.jobIconGreen;
      default:
        return undefined;
    }
  };

  return (
    <BoxContainer status={status} onClick={() => navigate(`detail-job/${id}`)}>
      <JobNameRow>
        <img
          src={getImageAcc(status)}
          alt={getImageAcc(status)}
          className="mr-10"
        />
        <JobName>{name}</JobName>
      </JobNameRow>
      <hr className="mt-10 mb-10" />
      <RowContainer>
        <TitleRow>{t("common.position")}</TitleRow>
        <ContentRow>{position}</ContentRow>
      </RowContainer>
      <RowContainer>
        <TitleRow>{t("common.level")}</TitleRow>
        <ContentRow>{level}</ContentRow>
      </RowContainer>
      <RowContainer>
        <TitleRow>{t("common.skill")}</TitleRow>
        <ContentRow>{skill}</ContentRow>
      </RowContainer>
      <RowContainer>
        <TitleRow>{t("managerJob.totalOrder")}</TitleRow>
        <ContentRow>{totalOrder}</ContentRow>
      </RowContainer>
      <RowContainer>
        <TitleRow>{t("managerJob.totalRecruited")}</TitleRow>
        <ContentRow>{totalCandidateOnboard + "/" + totalTarget}</ContentRow>
      </RowContainer>
      <hr className="mt-10 mb-10" />
      <RowContainer>
        <TitleRow>{t("common.status")}</TitleRow>
        <ContentRow status={status}>{getTextStatus(status)}</ContentRow>
      </RowContainer>
    </BoxContainer>
  );
};

export default BoxItemJob;
