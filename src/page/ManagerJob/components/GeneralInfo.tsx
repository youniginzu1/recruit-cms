import { useTranslation } from "react-i18next";
import styled from "styled-components";
import { colors } from "core/assets";

import { Grid } from "@mui/material";

import { getTextStatus, getColorStatusJob } from "../helpers";
import { formatSalary } from "core/helpers";

const Container = styled((props) => <Grid mt={1.5} container {...props}></Grid>)`
  border: 1px solid #e7e7e7;
  padding: 20px;
`;

const TitleRow = styled((props) => <Grid {...props}></Grid>)`
  color: #5a6882;
  font-size: 14px;
  font-weight: 400;
`;

const ContentRow = styled((props) => <Grid {...props}></Grid>)`
  color: ${({ status }) =>
    status ? getColorStatusJob(status) : colors.mainColor};
  font-size: 16;
  font-weight: 500;
`;

const RowInfoContainer = styled((props) => (
  <Grid container item xs={6} {...props}>
    <TitleRow item xs={4}>
      {props.title}
    </TitleRow>
    <ContentRow item xs={8} status={props?.status}>
      {props.content}
    </ContentRow>
  </Grid>
))``;

interface IProps {
  data: {
    level: string;
    position: string;
    skill: string;
    experience: string;
    maxSalary: string;
    totalOrder?: number;
    totalRecruited?: number;
    totalRequired?: number;
    status: number;
    description?: string;
  };
}

const GeneralInfo = (props: IProps) => {
  const { t } = useTranslation();
  const {
    level = "N/A",
    position = "N/A",
    skill = "N/A",
    experience = "N/A",
    maxSalary = "N/A",
    totalOrder = 0,
    totalRequired = 0,
    totalRecruited = 0,
    status,
    description = "N/A",
  } = props.data;

  return (
    <Container rowSpacing={1.5}>
      <RowInfoContainer title={t("common.position")} content={position} />
      <RowInfoContainer title={t("common.skill")} content={skill} />
      <RowInfoContainer title={t("common.level")} content={level} />
      <RowInfoContainer title={t("common.experience")} content={experience} />
      <RowInfoContainer
        title={t("managerJob.salaryUpTo")}
        content={formatSalary(maxSalary)}
      />
      <RowInfoContainer
        title={t("common.status")}
        content={getTextStatus(status)}
        status={status}
      />
      <RowInfoContainer
        title={t("managerJob.totalOrder")}
        content={totalOrder}
      />
      <RowInfoContainer
        title={t("managerJob.totalRecruited")}
        content={totalRecruited + "/" + totalRequired}
      />
      <RowInfoContainer title={t("form.description")} content={description} />
    </Container>
  );
};

export default GeneralInfo;
