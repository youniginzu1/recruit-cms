import { StatusJob } from "core/constants/enums";
import { colors } from "core/assets";
import { useTranslation } from "react-i18next";

export const getTextStatus = (status: number) => {
  const { t } = useTranslation();
  switch (status) {
    case StatusJob.PROCESSING:
      return t("managerJob.status.processing");
    case StatusJob.LATE_DEADLINE:
      return t("managerJob.status.lateDeadline");
    case StatusJob.COMPLETED:
      return t("managerJob.status.completed");
    default:
      return null;
  }
};

export const getColorStatusJob = (
  status: any,
  defaultColor: string = "#223354"
) => {
  if (!status) return defaultColor;
  switch (status) {
    case StatusJob.PROCESSING:
      return colors.processingColor;
    case StatusJob.LATE_DEADLINE:
      return colors.lateDeadlineColor;
    case StatusJob.COMPLETED:
      return colors.completedColor;
    default:
      return defaultColor;
  }
};

export const convertArrayOfArray = (
  parentArray: any = [],
  lengthSubArray: number
) => {
  let newArray = [];
  let subArray = [];
  let count = 0;
  while (count < parentArray.length) {
    if (count % lengthSubArray !== 0) subArray.push(parentArray[count]);
    else {
      if (count !== 0) newArray.push(subArray);
      subArray = [parentArray[count]];
    }
    count++;
  }
  newArray.push(subArray);
  return newArray;
};
