import { useState } from "react";
import { useTranslation } from "react-i18next";

import { Grid } from "@mui/material";
import { Button, Carousel, Spin, Empty } from "antd";

import styles from "./styles.module.scss";
import { IFilter } from "core/constants/interfaces";
import useFilter from "core/hooks/common/useFilter";
import { images } from "core/assets";
import { useGetListJob } from "core/hooks/job/useGetListJob";
import BoxItemJob from "./components/BoxItemJob";
import FilterJob from "./components/FilterJob";
import { convertArrayOfArray } from "./helpers";
import ModalAddJob from "./components/ModalAddJob";

const defaultFilter: IFilter = {};

interface IPropsCarousel {
  list: any;
  children: any;
}

const ManagerJob = () => {
  const { t } = useTranslation();
  const { filter, handleFilterChange, resetFilter, handleSearch } =
    useFilter(defaultFilter);

  const [openModalAddJob, setOpenModalAddJob] = useState(false);
  const { data: dataListJob, isLoading: isLoadingListJob } =
    useGetListJob(filter);

  const CarouselContainer = ({ list = [], children }: IPropsCarousel) => {
    if (list.length < 2) return children;
    return <Carousel autoplay>{children}</Carousel>;
  };

  return (
    <Grid container spacing={2} className={styles.managerJobContainer}>
      <Grid container item xs={12} className="d-flex-sb-c">
        <Grid item className={styles.titleHeader}>
          {t("managerJob.listJob")}
        </Grid>
        <Grid item>
          <Button
            className="btn mw-170"
            icon={<img src={images.addIcon} alt="" />}
            onClick={() => setOpenModalAddJob(true)}
          >
            {t("btn.addJob")}
          </Button>
        </Grid>
      </Grid>

      <Grid item container xs={9.5}>
        {isLoadingListJob && <Spin />}
        {!isLoadingListJob && dataListJob.data.length !== 0 && (
          <CarouselContainer list={convertArrayOfArray(dataListJob.data, 6)}>
            {convertArrayOfArray(dataListJob.data, 6).map((subArray, index) => (
              <Grid container key={index} className="d-flex" spacing={1.5}>
                {subArray.map((item, _index) => (
                  <Grid item xs={4} key={_index}>
                    <BoxItemJob info={item} />
                  </Grid>
                ))}
              </Grid>
            ))}
          </CarouselContainer>
        )}
        {!isLoadingListJob && dataListJob.data.length === 0 && (
          <div className="d-flex-c-c w-100">
            <Empty />
          </div>
        )}
      </Grid>
      <Grid item xs={2.5}>
        <FilterJob
          filter={filter}
          handleSearch={handleSearch}
          resetFilter={resetFilter}
          handleFilterChange={handleFilterChange}
          totalItems={dataListJob?.totalItems}
          metaData={dataListJob?.metadata}
        />
      </Grid>
      {openModalAddJob && (
        <ModalAddJob
          open={openModalAddJob}
          toggle={() => setOpenModalAddJob(false)}
        />
      )}
    </Grid>
  );
};

export default ManagerJob;
